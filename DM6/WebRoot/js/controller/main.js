angular.module('app').controller('mainCtrl', function($scope, $state, box, $translate) {
	$scope.logout = function() {
		box({
			title : '提示',
			content : '确定要退出系统吗?',
			scope : $scope,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					this.close();
					$state.go('login');
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ]
		});
	};
    $scope.switching = function(lang){
    	$translate.use(lang);
    };
});
