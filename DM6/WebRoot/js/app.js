var app = angular.module('app', ['ui.router', 'oc.lazyLoad', 'pascalprecht.translate', 'hljs']);
app.config(function($stateProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider, $translateProvider, hljsServiceProvider) {
	hljsServiceProvider.setOptions({
	    tabReplace: '    '
	});
	$translateProvider.preferredLanguage('cn');
	$translateProvider.useStaticFilesLoader({
		prefix: 'i18n/',
		suffix: '.json'
	});
	$urlRouterProvider.otherwise("/login");
    $stateProvider.state('login', {
    	url: "/login",
        controller: 'loginCtrl',
        templateUrl: 'login.html',
        resolve: {
        	load: ['$ocLazyLoad', function($ocLazyLoad) {
    			return $ocLazyLoad.load('js/controller/login.js');
        	}]
        }
	})
	.state('main', {
		url: "/",
		controller: 'mainCtrl',
		templateUrl: 'views/main.html',
		redirectTo: 'main.overview',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load(['js/service/ng-box.js', 'js/controller/main.js']);
			}]
		}
	})
	.state('main.overview', {
		url: "overview",
        templateUrl: 'views/overview/overview.html',
        resolve: {
        	load: ['$ocLazyLoad', function($ocLazyLoad) {
        	}]
        }
	})
	.state('main.alarm', {
		url: "alarmlist",
		templateUrl: 'views/alarm/alarm.html',
		redirectTo: 'main.alarm.alarmlist',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm.js');
			}]
		}
	})
	.state('main.alarm.alarmlist', {
		url: "",
		templateUrl: 'views/alarm/alarmlist.html',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarmlist.js');
			}]
		}
	})	
	.state('main.apitest', {
		url: "apitest",
		templateUrl: 'views/apitest/apitest.html',
		controller: 'apitestCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/apitest.js');
			}]
		}
	})	
	.state('main.guide', {
		url: "guide",
		templateUrl: 'views/guide/guide.html',
		controller: 'guideCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load(['plugins/highlight/styles/solarized-light.css', 'js/controller/guide.js']);
			}]
		}
	});
});
app.run(['$rootScope', '$state', function($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
      if (to.redirectTo) {
        evt.preventDefault();
        $state.go(to.redirectTo, params);
      }
    });
}]);