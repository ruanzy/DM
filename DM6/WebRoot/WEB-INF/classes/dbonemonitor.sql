create table if not exists alarm_type(
	id varchar(20) primary key,
	name varchar(20) not null
);
insert into alarm_type values('ENGINE', '引擎');
insert into alarm_type values('NODE', '节点');
insert into alarm_type values('SHARD', '数据库');

create table if not exists alarm_object(
	id varchar(20) primary key,
	name varchar(20) not null
);
insert into alarm_object values('CPU', 'CPU');
insert into alarm_object values('MEM', '内存');
insert into alarm_object values('DISK', '磁盘');
insert into alarm_object values('IO', '磁盘IO');
insert into alarm_object values('NETIO', '网络IO');

create table if not exists alarm_level(
	id varchar(20) primary key,
	name varchar(20) not null
);
insert into alarm_level values('ALERT', '提醒');
insert into alarm_level values('WARN', '警告');
insert into alarm_level values('ERROR', '严重');

create table if not exists alarm_strategy(
	id integer primary key AUTOINCREMENT,
	type varchar(20) not null,
	object varchar(20) not null,
	level varchar(20) not null,
	threshold varchar(20) not null,
	MEMO varchar(200)
);

create table if not exists alarm_noty_type(
	id integer primary key AUTOINCREMENT,
	name varchar(20) not null
);
insert into alarm_noty_type(name) values('Email');
insert into alarm_noty_type(name) values('SMS');

create table if not exists alarm_noty_person(
	id integer primary key AUTOINCREMENT,
	name varchar(20) not null
);
insert into alarm_noty_person(name) values('张三');

create table if not exists alarm_noty_person_type(
	personid integer not null,
	typeid integer not null,
	attr varchar(20) not null
);
insert into alarm_noty_person_type values(1, 1, '张三@163.com');
insert into alarm_noty_person_type values(1, 2, '13088888888');

create table if not exists alarm_strategy_noty(
	strategyid integer not null,
	personid integer not null,
	typeid integer not null,
	tmplate varchar(2000) not null
);