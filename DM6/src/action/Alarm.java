package action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import service.AlarmService;

import com.rz.common.Record;
import com.rz.web.Json;
import com.rz.web.View;
import com.rz.web.WebUtil;

public class Alarm
{
	public View execute(){
		AlarmService.ss();
		return null;
	}
	
	public View list()
	{
		Map<String, Object> ret = new HashMap<String, Object>();
		List<Map<String, Object>> list1 = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> list3 = new ArrayList<Map<String, Object>>();
		String page = WebUtil.getParameter("page");
		String pagesize = WebUtil.getParameter("pagesize");
		String type = WebUtil.getParameter("type");
		int p = Integer.valueOf(page);
		int ps = Integer.valueOf(pagesize);
		for (int i = 0; i < 88; i++)
		{
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", i + 1);
			if ((i + 1) % 8 == 0)
			{
				m.put("type", "SHARD");
			}
			else
			{
				m.put("type", "NODE");
			}
			m.put("time", "2016-04-18 15:58:55");
			m.put("level", new Random().nextInt(3) + 1);
			m.put("memo", "CPU过高(86%)");
			list1.add(m);
		}
		boolean isShard = ("SHARD").equals(type);
		for (int i = 0; i < 88; i++)
		{
			if (isShard)
			{
				if ((i + 1) % 8 == 0)
				{
					list2.add(list1.get(i));
				}
			}
			else
			{
				list2.add(list1.get(i));
			}
		}
		int total = list2.size();
		int begin = (p - 1) * ps;
		int end = p * ps;
		if (end >= total)
		{
			end = total;
		}
		for (int i = begin; i < end; i++)
		{
			list3.add(list2.get(i));
		}
		ret.put("total", total);
		ret.put("data", list3);
		ret.put("page", page);
		ret.put("pagesize", pagesize);
		return new Json(ret);
	}

	public View types()
	{
		List<Record> list = AlarmService.getTypes();
		return new Json(list);
	}

	public View objects()
	{
		List<Record> list = AlarmService.getObjects();
		return new Json(list);
	}

	public View levels()
	{
		List<Record> list = AlarmService.getLevels();
		return new Json(list);
	}

	public View strategys()
	{
		Map<String, String> condition = WebUtil.getParameters();
		List<Record> list = AlarmService.getStrategys(condition);
		return new Json(list);
	}

	public View addStrategy()
	{
		Map<String, Object> ret = new HashMap<String, Object>();
		Map<String, String> params = WebUtil.getParameters();
		AlarmService.addStrategy(params);
		ret.put("code", true);
		return new Json(ret);
	}
	
	public View notytypes()
	{
		List<Record> list = AlarmService.getNotyTypes();
		return new Json(list);
	}
	
	public View notypersons()
	{
		List<Record> list = AlarmService.getNotyPersons();
		return new Json(list);
	}
	
	public static class A{
		
	}
}
