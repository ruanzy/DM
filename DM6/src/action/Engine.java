package action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rz.web.Html;
import com.rz.web.Json;
import com.rz.web.View;

public class Engine
{
	public View execute()
	{
		//这个是默认执行的方法
		return new Html("engine.html");
	}

	public View list()
	{
		List<String> result = new ArrayList<String>();
		result.add("11.0.1.85");
		result.add("11.0.1.84");
		return new Json(result);
	}
	
	public View baseinfo(){
		//String ip = WebUtil.getParameter("ip");
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("ip", "11.0.1.85");
		m.put("pid", "9201");
		m.put("osName", "Linux");
		return new Json(m);
	}
}
