package action;

import java.util.ArrayList;
import java.util.List;

import com.rz.web.Html;
import com.rz.web.Json;
import com.rz.web.View;

public class Monitor
{
	public View execute()
	{
		return new Html("monitor.html");
	}

	public View nodes()
	{
		List<String> result = new ArrayList<String>();
		result.add("11.0.1.85");
		result.add("11.0.1.84");
		return new Json(result);
	}
	
	public View node()
	{
		//Map<String, Map<String, Object>> result = NodesMonitor.getInstance().getData();
		return new Json(null);
	}

	public View engine()
	{
		//Map<String, Object> result = EngineMonitor.getInstance().getData();
		return new Json(null);
	}
}
