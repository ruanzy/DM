package service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;

import com.rz.common.Record;
import com.rz.dao.DB;
import com.rz.dao.RowHandler;

public class AlarmService
{
	static DB db = DB.getInstance();

	public static List<Record> getTypes()
	{
		String sql = "SELECT * FROM alarm_type";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getObjects()
	{
		String sql = "SELECT * FROM alarm_object";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getLevels()
	{
		String sql = "SELECT * FROM alarm_level";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getStrategys(Map<String, String> condition)
	{
		String type = condition.get("type");
		String level = condition.get("level");
		String object = condition.get("object");
		StringBuffer sql = new StringBuffer("SELECT * FROM alarm_strategy where 1=1");
		if (StringUtils.isNotBlank(type))
		{
			sql.append(" and type='").append(type).append("'");
		}
		if (StringUtils.isNotBlank(object))
		{
			sql.append(" and object='").append(object).append("'");
		}
		if (StringUtils.isNotBlank(level))
		{
			sql.append(" and level='").append(level).append("'");
		}
		return DB.getInstance().find(sql.toString(), new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String type = rs.getString("type");
				String object = rs.getString("object");
				String level = rs.getString("level");
				int threshold = rs.getInt("threshold");
				String memo = rs.getString("memo");
				r.put("id", id);
				r.put("type", type);
				r.put("object", object);
				r.put("level", level);
				r.put("threshold", threshold);
				r.put("memo", memo);
				return r;
			}
		});
	}

	public static void addStrategy(Map<String, String> params)
	{
		String sql = "INSERT INTO alarm_strategy(type, object, level, threshold, memo) VALUES(?, ?, ?, ?, ?)";
		String type = params.get("type");
		String object = params.get("object");
		String level = params.get("level");
		int threshold = Integer.valueOf(params.get("threshold"));
		String memo = params.get("memo");
		DB db = DB.getInstance();
		db.begin();
		Object[] p = new Object[] { type, object, level, threshold, memo };
		db.update(sql, p);
		db.commit();
	}

	public static List<Record> getNotyTypes()
	{
		String sql = "SELECT * FROM alarm_noty_type";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getNotyPersons()
	{
		String sql = "SELECT * FROM alarm_noty_person";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}
	
	public static void ss()
	{
		ExecutorService executor = Executors.newCachedThreadPool();
        Future<Integer> result = executor.submit(new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				// TODO Auto-generated method stub
				return null;
			}
		});
        executor.shutdown();
	}
}
