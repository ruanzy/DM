package com.rz.web;

import javax.servlet.ServletContext;

public interface Starter
{
	public void start(ServletContext context);
}
