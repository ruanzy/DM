package com.rz.web;

public interface Interceptor
{
	void intercept(Action action);
}
