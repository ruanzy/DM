package test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.apache.taglibs.standard.lang.jstl.test.Bean1;

import action.Alarm;

public class Test {

	public static void main(String[] args) {
		String className = "action.Alarm$A";
		Object o = loadStaticInnerClass(className);
		System.out.println(o);
	}
	
	public static Object loadStaticInnerClass(String className)
	{
		Object o = null;
		try {
			Class<?> cla = Class.forName(className);
			o = cla.newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;
	}
	
	public static Object loadInnerClass(String className)
	{
		Object o = null;
		try {
			Class<?> cla = Class.forName(className);
			Constructor<?>[] c = cla.getDeclaredConstructors();
			int i = c[0].getModifiers();
			//得到访问修饰符
			String str = Modifier.toString(i);
			System.out.println(str+" aaaaaaaaa");
			o = (Bean1)c[0].newInstance();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return o;
	}

}
