(function($) {
    $.fn.datepicker = function(method) {
		if (typeof method == 'string') {
			return arguments.callee.methods[method].apply(this,
					Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return arguments.callee.methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist');
		}
	};
	$.fn.datepicker.defaults = {
		format:'yyyy-MM-dd',
		onpicked:function(){
			
		}
	};
	$.fn.datepicker.methods = {
		init: function(opts) {
			var _opts = $.extend({}, $.fn.datepicker.defaults, opts);
			return this.each(function(){
				var me = $(this);//.attr('readonly', 'readonly');
				var dl = $("<dl class='datepicker'><dt></dt><dd></dd></dl>");
				me.wrap(dl);
				me.after('<div style="position:absolute;top:0;right:0;bottom:0;left:0;"></div>');
				var dt = me.parent('dt');
				var dd = dt.siblings('dd');
				dd.append(tpl());
				var c = dd.find('.calender');
				var c_head = c.find('.calender-head');
				var c_body = c.find('.calender-body');
				var c_curr = c_head.find('.curr');
				var sel = c_head.find('.sel');
				c_body.delegate('td', "click", function(e) {
					e.stopPropagation();
					var mode = c.data('mode');
					if(mode == 'day'){
						if($(this).hasClass('day')){
							var d = $(this).text();
							var y = c.data('year');
							var m = c.data('month');
							var v = new Date(y, m, d);
							me.val(format(v, _opts.format));
							_opts.onpicked.call(me);
							c.hide();
							dd.hide();
						}
					}
					if(mode == 'month'){
						var m = parseInt($(this).text());
						var y = c.data('year');
						c_body.html(getDays(y, m - 1));
						c.data('month', m - 1);
						c.data('mode', 'day');
						c_curr.find('a').html(y  + '年' + m + '月');
					}
					if(mode == 'year'){
						var y = parseInt($(this).text());
						c_body.html(getMonths());
						c.data('year', y);
						c.data('mode', 'month');
						c_curr.find('a').html(y);
					}
				});	
				sel.delegate('li', "click", function(e) {
					e.stopPropagation();
					var mode = c.data('mode');
					if($(this).hasClass('next')){
						if(mode == 'day'){
							var y = c.data('year');
							var m = c.data('month');
							y = (m == 11)? (y + 1) : y;
							m = (m == 11)? 0: (m + 1);
							c_body.html(getDays(y, m));
							c.data('year', y);
							c.data('month', m);
							c_curr.find('a').html(y  + '年' + (m + 1) + '月');
						}
						if(mode == 'year'){
							var y = c.data('year');
							c_body.html(getYears(y));
							c_curr.find('a').html(getYearsRange(y));
						}
						if(mode == 'month'){
							var y = c.data('year');
							c_curr.find('a').html(++y);
							c.data('year', y);
						}
					}
					if($(this).hasClass('prev')){
						if(mode == 'day'){
							var y = c.data('year');
							var m = c.data('month');
							y = (m == 0)? (y - 1) : y;
							m = (m == 0)? 11: (m - 1);
							c_body.html(getDays(y, m));
							c.data('year', y);
							c.data('month', m);
							c_curr.find('a').html(y  + '年' + (m + 1) + '月');
						}
						if(mode == 'month'){
							var y = c.data('year');
							c_curr.find('a').html(--y);
							c.data('year', y);
						}
					}
					if($(this).hasClass('curr')){
						if(mode == 'day'){
							c_body.html(getMonths());
							var y = c.data('year');
							c_curr.find('a').html(y);
							c.data('mode', 'month');
						}
						if(mode == 'month'){
							var y = c.data('year');
							c_body.html(getYears(y));
							c_curr.find('a').html(getYearsRange(y));
							c.data('mode', 'year');
						}
					}
				});	
				
				$(document).click(function() {
					c.hide();
					dd.hide();
				});	
				$(document).bind("mouseup", function(e) {
					c.hide();
					dd.hide();
				});	
				c.click(function(e){
					e.stopPropagation();
				});	
				c.mouseup(function(e){
					e.stopPropagation();
				});		
				dt.click(function(e){
					e.stopPropagation();
					c.data('mode', 'day');
					var now = new Date();
					var y = now.getFullYear();
					var m = now.getMonth();
					c_body.html(getDays(y, m));
					c.data('year', y);
					c.data('month', m);
					c_curr.find('a').html(y  + '年' + (m + 1) + '月');
					c.show();
					dd.show();
				});	
			});
		}
	};
	function format(date, format){ 
		if (!(date instanceof Date)) {
			return;
		}
		var o = { 
			"M+" : date.getMonth()+1,
			"d+" : date.getDate(),
			"h+" : date.getHours(),
			"m+" : date.getMinutes(),
			"s+" : date.getSeconds(),
			"q+" : Math.floor((date.getMonth()+3)/3),
			"S" : date.getMilliseconds()
		}; 
		if(/(y+)/.test(format)) { 
			format = format.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
		} 		
		for(var k in o) { 
			if(new RegExp("("+ k +")").test(format)) { 
				format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
			} 
		} 
		return format; 
	}
	function getDaysInMonth(year, month) {
		var isLeapYear = (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
		return [31, (isLeapYear ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
	}
	function firstWeekDay(year, month) {
		return new Date(year, month, 1).getDay();
	}	
	var weekarr = ['日', '一', '二', '三', '四', '五', '六'];
	function getDays(y, m){
		var daysInMonth = getDaysInMonth(y, m);
		var _first_week_day = firstWeekDay(y, m);
		var html = [];
		html.push('<table class="daytable">');
		html.push('<tr>');
		for(i = 0; i < 7; i++) {
			html.push('<td height="26">', weekarr[i], '</td>');
		}
		html.push('<tr>');
		for(i = 0; i < _first_week_day; i++) {
			html.push('<td class="empty"></td>');
		}
		var week_day = _first_week_day;
		for (i = 1; i <= daysInMonth; i++) {
			week_day %= 7;
			if(week_day == 0){
				html.push('<tr>');	
			}
			html.push('<td class="day"><a href="javascript:void(0)">', i, '</a></td>');
			if(week_day == 6){
				html.push('</tr>');	
			}
			week_day++;
		}
		html.push('</table>');
		return 	html.join('');	
	}
	function getYears(y){
		var _t = Math.floor(y / 10); 
		var start = _t*10;
		var end = start + 19;
		var html = [];
		html.push('<table class="table table-bordered">');
		html.push('<tr>');
		var week_day = 0;
		for (i = start; i <= end; i++) {
			week_day %= 4;
			if(week_day == 0){
				html.push('<tr>');	
			}
			html.push('<td class="year"><a href="javascript:void(0)">', i, '</a></td>');
			if(week_day == 3){
				html.push('</tr>');	
			}
			week_day++;
		}
		html.push('</table>');
		return 	html.join('');	
	}
	function getYearsRange(y){
		var _t = Math.floor(y / 10); 
		var start = _t*10;
		var end = start + 19;
		return start + '-' + end;
	}
	function getMonths(){
		var html = [];
		html.push('<table class="table table-bordered">');
		html.push('<tr>');
		var week_day = 0;
		for (var i = 1; i <= 12; i++) {
			week_day %= 4;
			if(week_day == 0){
				html.push('<tr>');	
			}
			html.push('<td class="month"><a href="javascript:void(0)">', i, '月</a></td>');
			if(week_day == 3){
				html.push('</tr>');	
			}
			week_day++;
		}
		html.push('</table>');
		return 	html.join('');	
	}
	function tpl(){
		var html = [];
		html.push('<div class="calender">');
		html.push('<div class="calender-head">');
		html.push('<ul class="sel"><li class="prev"><a href="javascript:void(0)"><</a></li><li class="curr"><a href="javascript:void(0)"></a></li><li class="next"><a href="javascript:void(0)">></a></li></ul>');
		html.push('</div>');
		html.push('<div class="calender-body"></div>');
		html.push('<div  class="calender-foot">');
		html.push('<div><button>Today</button><button>Clear</button></div>');
		html.push('</div>');
		html.push('</div>');
		return 	html.join('');	
	}
})(jQuery);
angular.module("datepicker", []).directive('datepicker', function() {
	var directive = {};
	directive.restrict = 'A';
	directive.scope = {
		ngModel: '='
	};
	directive.compile = function compile(element, attrs, transclude) {
		return function(scope, elem, attrs){
			var e = $(elem);
			e.datepicker({
				onpicked : function(){
					scope.$apply(function(){
						scope.ngModel = e.val();
					});
				}
			});
		};
	};
	directive.link = function compile(scope, element, attrs) {
	};
	return directive;
});