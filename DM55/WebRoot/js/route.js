app.config(function($stateProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider) {
	$urlRouterProvider.otherwise("/login");
    $stateProvider.state('login', {
    	url: "/login",
        controller: 'loginCtrl',
        templateUrl: 'login.html',
        resolve: {
        	load: ['$ocLazyLoad', function($ocLazyLoad) {
    			return $ocLazyLoad.load(['js/service/ng-base64.js', 'js/controller/login.js']);
        	}]
        }
	})
	.state('main', {
		url: "/",
		controller: 'mainCtrl',
		templateUrl: 'views/main.html',
		redirectTo: 'main.overview',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load(['js/service/ng-box.js', 'js/controller/main.js', 'grid', 'select2', 'datepicker']);
			}]
		}
	})
	.state('main.overview', {
		url: "overview",
        templateUrl: 'views/overview/overview.html',
        controller: 'overviewCtrl',
        resolve: {
        	load: ['$ocLazyLoad', function($ocLazyLoad) {
        		return $ocLazyLoad.load(['js/controller/overview.js']);
        	}]
        }
	})
	.state('main.nodes', {
		url: "nodes",
        templateUrl: 'views/node/nodes.html',
        redirectTo: 'main.nodes.nodelist',
        resolve: {
        	load: ['$ocLazyLoad', function($ocLazyLoad) {
        		return $ocLazyLoad.load(['js/controller/node/nodes.js']);
        	}]
        }
	})
	.state('main.nodes.nodelist', {
		url: "",
		templateUrl: 'views/node/nodeList.html',
		controller: 'nodeListCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/node/nodeList.js');
			}]
		}
	})
	.state('main.alarm', {
		url: "alarm",
		templateUrl: 'views/alarm/alarm.html',
		redirectTo: 'main.alarm.alarmlist'
	})
	.state('main.alarm.alarmlist', {
		url: "",
		templateUrl: 'views/alarm/alarmList.html',
		controller: 'alarmListCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmList.js');
			}]
		}
	})	
	.state('main.alarm.alarmhandle', {
		url: "",
		templateUrl: 'views/alarm/alarmHandle.html',
		controller: 'alarmHandleCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmHandle.js');
			}]
		}
	}).state('main.alarm.alarmtype', {
		url: "",
		templateUrl: 'views/alarm/alarmType.html',
		controller: 'alarmTypeCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmType.js');
			}]
		}
	})	
	.state('main.alarm.alarmobject', {
		url: "",
		templateUrl: 'views/alarm/alarmObject.html',
		controller: 'alarmObjectCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmObject.js');
			}]
		}
	}).state('main.alarm.alarmlevel', {
		url: "",
		templateUrl: 'views/alarm/alarmLevel.html',
		controller: 'alarmLevelCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmLevel.js');
			}]
		}
	})	
	.state('main.alarm.alarmStrategy', {
		url: "",
		templateUrl: 'views/alarm/alarmStrategy.html',
		controller: 'alarmStrategyCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmStrategy.js');
			}],
			types:  function($http){
	            return $http({method: 'GET', url: 'alarm/types'});
	         },
			 objects:  function($http){
	            return $http({method: 'GET', url: 'alarm/objects'});
	         },
	         levels:  function($http){
		            return $http({method: 'GET', url: 'alarm/levels'});
	         },
	         notytypes:  function($http){
	        	 return $http({method: 'GET', url: 'alarm/notytypes'});
	         },
	         notypersons:  function($http){
	        	 return $http({method: 'GET', url: 'alarm/notypersons'});
	         }
		}
	})
	.state('main.alarm.alarmNoty', {
		url: "",
		templateUrl: 'views/alarm/alarmNoty.html',
		controller: 'alarmNotyCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/alarm/alarmNoty.js');
			}],
			notycfg:  function($http){
				return $http({method: 'GET', url: 'alarm/getNotyCfg'});
			}
		}
	})
	.state('main.apitest', {
		url: "apitest",
		templateUrl: 'views/apitest/apitest.html',
		controller: 'apitestCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
				return $ocLazyLoad.load('js/controller/apitest.js');
			}]
		}
	})	
	.state('main.guide', {
		url: "guide",
		templateUrl: 'views/guide/guide.html',
		controller: 'guideCtrl',
		resolve: {
			load: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load("highlight").then(
                    function(){
                        return $ocLazyLoad.load(["plugins/highlight/angular-highlightjs.js", "js/controller/guide.js"]);
                    }
                );
			}]
		}
	});
});