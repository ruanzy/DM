angular.module('app').controller('alarmStrategyCtrl', function($scope, box, types, objects, levels, notytypes, notypersons) {
	$scope.info = {
			content : "hello,world"
	};
	$scope.config = {width: '100px'};
	$scope.noty = {};
	$scope.notytypecfg = {
        data: notytypes.data
    };
	var notypersonsdata = $.map(notypersons.data, function (obj) {
	  obj.text = obj.text || obj.name; 
	  return obj;
	});
	$scope.notypersonscfg = {
		tags: true,
        data: notypersonsdata
    };
	$scope.typecfg = {
		data : types.data
	};
	$scope.objectcfg = {
		data : objects.data
	};
	$scope.levelcfg = {
		data : levels.data
	};
	$scope.relationcfg = {
		data : [
		        {id : ">", text : "大于"},
		        {id : ">=", text : "大于等于"},
		        {id : "=", text : "等于"},
		        {id : "<", text : "小于"},
		        {id : "<=", text : "小于等于"}
		]
	};
	$scope.gridconfig = {
		title : '告警策略列表',
		url : 'alarm/strategys',
		pager : false,
		condition : function(){return $scope.query;},
		processData : function(data){
			var ret = {total:0, data:[]};
			ret['total'] = data.length;
			ret['data'] = data;
			return ret;
		},
		columns :[
		     {header :'ID', field: 'id', width : 100, align : 'center'},
		     {header :'触发条件', field: 'threshold', align : 'center', render : triggerRender},
		     //{header :'告警对象', field: 'object', render : objectRender, align : 'center'},
		     {header :'告警类型', field: 'type', align : 'center'},
		     {header :'告警级别', field: 'level', render : levelRender, align : 'center'}
		]
	};
	function triggerRender(threshold, r){
		var ret = [];
		var arr = objects.data;
		var relation = r.relation;
		var object = r.object;
		var objectname = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == object){
				objectname = arr[k].text;
			}
		}
		ret.push(objectname, relation, threshold);
		return ret.join(' ');
	}
	function levelRender(level){
		var arr = levels.data;
		var ret = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == level){
				ret = arr[k].text;
			}
		}
		return ret;
	}
	function objectRender(obj){
		var arr = objects.data;
		var ret = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == obj){
				ret = arr[k].text;
			}
		}
		return ret;
	}
	$scope.query = {};
	$scope.data = {};
	$scope.search = function() {
		$scope.gridconfig.getGrid().grid('reload', $scope.query);
	};
	$scope.addStrategy = function() {
		box({
			title : '新增告警策略',
			url : 'views/alarm/addAlarmStrategy.html',
			scope : $scope,
			width : 600,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					var data = $scope.data;
					addStrategy(data);
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ],
			open : function() {
				
			}
		});
	};
	
	function addStrategy(data){
		$.ajax({
			url: 'alarm/addStrategy',
			type: 'post',
			data: data,
			dataType: 'json',
	        success: function(result){
	        	$scope.gridconfig.getGrid().grid('reload', $scope.query);
			}
		});
	}
	
	$scope.setNoty = function() {
		box({
			title : '设置告警通知',
			url : 'views/alarm/setAlarmNoty.html',
			scope : $scope,
			width : 600,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					var data = $scope.data;
					setNoty(data);
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ],
			open : function() {
				
			}
		});
	};
	
	function setNoty(data){
		$.ajax({
			url: 'alarm/setNoty',
			type: 'post',
			data: data,
			dataType: 'json',
	        success: function(result){
	        	//$scope.gridconfig.getGrid().table('reload', $scope.query);
			}
		});
	}
});