angular.module('app').controller('overviewCtrl', function($scope, $state) {
	$scope.url = 'engine/list';//引擎数据的请求路径
	$scope.url_app = 'app/list';//应用数据的请求路径
	$scope.url_node = 'node/list';//节点数据的请求路径
	$scope.url_shard = 'shard/list';//shard数据的请求路径
	
	$scope.result_engine = {};
	$scope.result_app = {};
	$scope.result_node = {};
	$scope.result_shard = {};
	
	$scope.nodeisShow = false;//node默认是隐藏的
	$scope.shardisShow = false;//node默认是隐藏的
	
	$scope.isActive = false;//是否在点击状态
	
	$scope.currentAppName = "";
	$scope.currentNodeId = "";
	
	$scope.getNodesInfo = getNodesInfo;
	$scope.getShardInfo = getShardInfo;
	
	//根据状态判断样式
	$scope.setStateStyle = function(args) {
		if(args == 1){
			return 'state1';
		}else if(args ==2) {
			return 'state2';
		}else if(args == 3) {
			return 'state3'; 
		};	
	};
	
	//直接执行
	getEngineInfo();
	getAppList();
	
	//获取引擎信息的方法
	function getEngineInfo() {
		var data = {};
		$.ajax({
			url : $scope.url,
			type : 'get',
			data : data,
			dataType : 'json',
			success : function(ret) {
				$scope.$apply(function(){
					$scope.result_engine = ret;
				});
			}
		});
	};
	
	//获取应用数据的方法
	function getAppList(){
		/***
		var data = {};
		$.ajax({
			url : $scope.url_app,
			type : 'get',
			data : data,
			dataType : 'json',
			success : function(ret) {
				$scope.$apply(function(){
					$scope.result_app = ret;
				});
			}
		});
		***/
		$scope.result_app = [{"name":"app1","state":"1","active":true,"createdOn":"2016-04-2116:19:14","createdBy":"dbone","redundance":0,"supportGIS":false},{"name":"app2","state":"2","active":true,"createdOn":"2016-04-2216:19:14","createdBy":"dbone","redundance":1,"supportGIS":false}];
		//这里取数据的第一条，默认展开数据
		$scope.currentAppName = $scope.result_app[0].name;
		getNodesInfo($scope.currentAppName);
	};
	
	//
	function getNodesInfo(appname){
		$scope.currentAppName = appname;
		/***
		var data = appname;
		data.appName = appname;
		$.ajax({
			url : $scope.url_node,
			type : 'post',
			data : data,
			dataType : 'json',
			success : function(ret) {
				$scope.$apply(function(){
					$scope.result_node = ret;
					//获取app信息的时候，因为shard的数据还没选，这里暂时取空。
					$scope.result_shard = {};
					//默认展开第一个node的所有shard
					getShardInfo(appname,$scope.result_node[0].nodeId);
				});
			}
		});
		***/
		
		if(appname == "app1"){
			$scope.result_node = [{"id":2,"state":"1","nodeId":"11.0.1.85","ip":"11.0.1.85","heartip":"","username":"idb","password":"idb","sshPort":22,"idbdir":"\/usr\/local\/idb","status":true,"appNodeServPort":29000,"weight":1,"idbinstalled":false},{"id":1,"state":"2","nodeId":"11.0.1.84","ip":"11.0.1.84","heartip":"","username":"idb","password":"idb","sshPort":22,"idbdir":"\/usr\/local\/idb","status":true,"appNodeServPort":29000,"weight":1,"idbinstalled":false}];
		}else{
			$scope.result_node = [{"id":3,"state":"1","nodeId":"11.0.1.87","ip":"11.0.1.87","heartip":"","username":"idb","password":"idb","sshPort":22,"idbdir":"\/usr\/local\/idb","status":true,"appNodeServPort":29000,"weight":1,"idbinstalled":false},{"id":1,"state":"2","nodeId":"11.0.1.88","ip":"11.0.1.88","heartip":"","username":"idb","password":"idb","sshPort":22,"idbdir":"\/usr\/local\/idb","status":true,"appNodeServPort":29000,"weight":1,"idbinstalled":false}];
		}
		//获取app信息的时候，因为shard的数据还没选，这里暂时取空。
		$scope.result_shard = {};
		//默认展开第一个node的所有shard
		getShardInfo(appname,$scope.result_node[0].nodeId);
	};
	
	//
	function getShardInfo(appname,nodeId){
		$scope.currentAppName = appname;
		$scope.currentNodeId = nodeId;
		/***
		var data = {"appname":appname,"nodeId":nodeId};
		$.ajax({
			url : $scope.url_shard,
			type : 'post',
			data : data,
			dataType : 'json',
			success : function(ret) {
				$scope.$apply(function(){
					$scope.result_shard = ret;
				});
			}
		});
		***/
		if(nodeId == "11.0.1.85"){
			$scope.result_shard = [{state:"1",appName:"intple_yb",createdTime:"2016-04-2116:19:59",creator:"dbone",id:3,instanceId:"ins_11.0.1.36_35210",isActive:true,physicalShardId:"intple_yb_0001",redundance:0,running:true}];
		}else if(nodeId == "11.0.1.87"){
			$scope.result_shard = [{state:"1",appName:"intple_yb",createdTime:"2016-04-2116:19:59",creator:"dbone",id:3,instanceId:"ins_11.0.1.36_35210",isActive:true,physicalShardId:"intple_yb_6545",redundance:0,running:true}];
		}else if(nodeId == "11.0.1.88"){
			$scope.result_shard = [{state:"1",appName:"intple_yb",createdTime:"2016-04-2116:19:59",creator:"dbone",id:3,instanceId:"ins_11.0.1.36_50",isActive:true,physicalShardId:"intple_yb_50",redundance:0,running:true}];
		}else{
			$scope.result_shard = [{state:"1",appName:"intple_yb222",createdTime:"2016-04-2116:19:59",creator:"dbone2",id:3,instanceId:"ins_11.0.1.36_20",isActive:true,physicalShardId:"intple_yb_0003321",redundance:0,running:true},{state:"2",physicalShardId:"intple_yb_1"},{state:"3",physicalShardId:"intple_yb_2"},{state:"1",physicalShardId:"intple_yb_3"},{state:"1",physicalShardId:"intple_yb_4"}];
		}
	};
	
	//首次页面展示第一个应用的第一个节点的数据
	function getFirstNodeData(data){
		$.each(data,function(index,d){
			if(index == 0){
				
			}
		});
	};
	
});
