angular.module('app').controller('alarmStrategyCtrl', function($scope, box, types, objects, levels, notytypes, notypersons, $http) {
	$scope.info = {
			content : "hello,world"
	};
	$scope.config = {width: '100px'};
	$scope.noty = {};
	$scope.notytypecfg = {
        data: notytypes.data
    };
	var notypersonsdata = $.map(notypersons.data, function (obj) {
	  obj.text = obj.text || obj.name; 
	  return obj;
	});
	$scope.notypersonscfg = {
		tags: true,
        data: notypersonsdata
    };
	$scope.typecfg = {
		data : types.data
	};
	$scope.objectcfg = {
		data : objects.data
	};
	$scope.levelcfg = {
		data : levels.data
	};
	$scope.relationcfg = {
		data : [
		        {id : ">", text : "大于"},
		        {id : ">=", text : "大于等于"},
		        {id : "=", text : "等于"},
		        {id : "<", text : "小于"},
		        {id : "<=", text : "小于等于"}
		]
	};
	$scope.gridconfig = {
		title : '告警策略列表',
		url : 'alarm/strategys',
		pager : false,
		condition : function(){return $scope.query;},
		processData : function(data){
			var ret = {total:0, data:[]};
			ret['total'] = data.length;
			ret['data'] = data;
			return ret;
		},
		columns :[
		     {header :'ID', field: 'id', width : 100, align : 'center'},
		     {header :'触发条件', field: 'threshold', align : 'center', render : triggerRender},
		     //{header :'告警对象', field: 'object', render : objectRender, align : 'center'},
		     {header :'告警类型', field: 'type', align : 'center'},
		     {header :'告警级别', field: 'level', render : levelRender, align : 'center'},
		     {header :'操作', field: 'op', render : opRender, align : 'center'}
		]
	};
	function triggerRender(threshold, r){
		var ret = [];
		var arr = objects.data;
		var relation = r.relation;
		var object = r.object;
		var times = r.times;
		var objectname = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == object){
				objectname = arr[k].text;
			}
		}
		ret.push(objectname, relation, threshold);
		var str = ret.join(' ') + '(' + times + '次以上)';
		return str;
	}
	function levelRender(level){
		var arr = levels.data;
		var ret = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == level){
				ret = arr[k].text;
			}
		}
		return ret;
	}
	function objectRender(obj){
		var arr = objects.data;
		var ret = '';
		for(var k = 0, len = arr.length; k < len; k++){
			if(arr[k].id == obj){
				ret = arr[k].text;
			}
		}
		return ret;
	}
	function opRender(v, r){
		var id = r.id;
		return '<a ng-click="setNoty(\'' + id + '\')">设置告警通知</a>';
	}
	$scope.query = {};
	$scope.data = {};
	$scope.search = function() {
		$scope.gridconfig.getGrid().grid('reload', $scope.query);
	};
	$scope.addStrategy = function() {
		box({
			title : '新增告警策略',
			url : 'views/alarm/addAlarmStrategy.html',
			scope : $scope,
			width : 600,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					var data = $scope.data;
					addStrategy(data);
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ],
			open : function() {
				
			}
		});
	};
	
	function addStrategy(data){
		$.ajax({
			url: 'alarm/addStrategy',
			type: 'post',
			data: data,
			dataType: 'json',
	        success: function(result){
	        	$scope.gridconfig.getGrid().grid('reload', $scope.query);
			}
		});
	}
	
	$scope.setNoty = function(id) {
		$http.post('alarm/getStrategyNoty', {id : id})
	    .success(function(data, status, headers, config){
	    	var persons = data.persons.split(',');
	    	var personsitem = [];
	    	for(var k in notypersonsdata){
	    		var p = notypersonsdata[k];
	    		if($.inArray(p.id, persons) > -1){
	    			personsitem.push(p);
	    		}
	    	}
			$scope.noty = {
				id : id,
				persons : data.persons,
				personsitem : personsitem,
				tmpl : data.notytmpl
			};
			box({
				title : '设置告警通知',
				url : 'views/alarm/setStrategyNoty.html',
				scope : $scope,
				width : 600,
				drag : true,
				buttons : [ {
					text : '确定',
					cls : 'btn-primary',
					action : function() {
						var data = $scope.noty;
						setStrategyNoty(data);
						this.close();
					}
				}, {
					text : '关闭',
					cls : 'btn-default',
					action : function() {
						this.close();
					}
	
				} ],
				open : function() {
					
				}
			});
	    })
	    .error(function(data, status, headers, config){

	    });
	};
	
	function setStrategyNoty(data){
		$.ajax({
			url: 'alarm/setStrategyNoty',
			type: 'post',
			data: data,
			dataType: 'json',
	        success: function(result){
	        	//$scope.gridconfig.getGrid().table('reload', $scope.query);
			}
		});
	}
});