angular.module('app').controller('alarmObjectCtrl', function($scope, $state) {
	$scope.gridconfig = {
		title : '告警对象列表',
		url : 'alarm/objects',
		pager : false,
		processData : function(data){
			var ret = {total:0, data:[]};
			ret['total'] = data.length;
			ret['data'] = data;
			return ret;
		},
		columns :[
		     {header :'ID', field: 'id'},
		     {header :'告警对象', field: 'text'}
		]
	};
	$scope.data = {};
	$scope.query = function(){
		$scope.gridconfig.getGrid().table('reload', $scope.data);
	};
});
