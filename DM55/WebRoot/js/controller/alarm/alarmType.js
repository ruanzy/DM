angular.module('app').controller('alarmTypeCtrl', function($scope, $state, box) {
	$scope.gridconfig = {
		title : '告警类型列表',
		url : 'alarm/types',
		pager : false,
		processData : function(data){
			var ret = {total:0, data:[]};
			ret['total'] = data.length;
			ret['data'] = data;
			return ret;
		},
		columns :[
		     {header :'ID', field: 'id'},
		     {header :'告警类型', field: 'text'}
		]
	};
	$scope.data = {};
	$scope.query = function(){
		$scope.gridconfig.getGrid().table('reload', $scope.data);
	};
});
