angular.module('app').controller('alarmHandleCtrl', function($scope, $state, box) {
   	$scope.typecfg = {
        data: [{id:'SHARD',text:'数据库'}, {id:'NODE',text:'节点'}, {id:'ENGINE',text:'引擎'}]
    };
	$scope.data = {};
	$scope.query = function(){
		$scope.gridconfig.getGrid().grid('reload', $scope.data);
	};
	$scope.gridconfig = {
   		title : '告警列表',
   		sm : 'm',
   		url : 'alarm/list',
   		pagesize : 5,
   		condition : function(){return $scope.data;},
   		columns :[
   		     {header :'ID', field: 'id', width : 100, align : 'center'},
   		     {header :'告警时间', field: 'time', width : 200, align : 'center'},
   		     {header :'告警类型', field: 'type', width : 100, align : 'center'},
   		     {header :'告警级别', field: 'level', render : levelRender, width : 100, align : 'center'},
   		     {header :'详情', field: 'memo'},
   		     {header :'状态', field: 'state', render : stateRender, width : 100, align : 'center'}
   		]
   	};
   	function levelRender(level){
   		var arr1 = ['info', 'warning', 'danger'];
   		var arr2 = ['提示', '警告', '危险'];
   		return '<span class="label label-' + arr1[level -1] + '">' + arr2[level -1] +'</span>';
   	}
   	function stateRender(state, r){
//   		var arr1 = ['info', 'warning', 'danger'];
//   		var arr2 = ['已忽略', '未处理', '已处理', '已关闭'];
   		var alarmId = r.id;
   		return '<a ng-click="handleAlarm(\'' + alarmId + '\')">处理</a>';
   	}
   	$scope.handleAlarm = function(alarmId){
   		box({
			title : '提示',
			content : '确定要处理' + alarmId + '吗?',
			scope : $scope,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ]
		});
	};
});
