angular.module('app').controller('apitestCtrl', function($scope, $state) {
	$scope.url = 'engine/baseinfo';
	$scope.params = '{"ip" : "11.0.1.85"}';
	$scope.result = '';
	$scope.reset = function() {
		$scope.url = 'engine/baseinfo';
		$scope.params = '{"ip" : "11.0.1.85"}';
		$scope.result = '';
	};
	$scope.reset();
	$scope.send = function() {
		var data = {};
		if($scope.params){
			data = $.parseJSON($scope.params);
		}
		$.ajax({
			url : $scope.url,
			type : 'post',
			data : data,
			dataType : 'json',
			success : function(ret) {
				$scope.$apply(function(){
					$scope.result = ret;
				});
			}
		});
	};
});