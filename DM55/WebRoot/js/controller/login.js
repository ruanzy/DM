angular.module('app').controller('loginCtrl', function($scope, $state, $base64, $http, $window) {
	$scope.formdata = {
		username : 'admin',
		password : 'admin'
	};

	$scope.login = function() {
		var p = {
			username : $scope.formdata.username,
			password : $base64.encode($scope.formdata.password)	
		};
		$http.post('Login/userLogin', p).success(function(data) {
			$window.sessionStorage["token"] = data.token;
			$state.go('main');
		});
	};
});
