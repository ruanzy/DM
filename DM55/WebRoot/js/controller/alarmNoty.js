angular.module('app').controller('alarmNotyCtrl', function($scope, box, notycfg, $http) {
	$scope.gridconfig = {
		title : '告警通知人员列表',
		url : 'alarm/notypersons',
		pager : false,
		processData : function(data){
			var ret = {total:0, data:[]};
			ret['total'] = data.length;
			ret['data'] = data;
			return ret;
		},
		columns :[
		     {header :'ID', field: 'id', width : 100, align : 'center'},
		     {header :'姓名', field: 'name', align : 'center'},
		     {header :'Email', field: 'email', align : 'center'}
		]
	};
	$scope.search = function() {
		$scope.gridconfig.getGrid().grid('reload');
	};
	$scope.data = {};
	$scope.addPerson = function() {
		box({
			title : '新增通知人员',
			url : 'views/alarm/addAlarmNotyPerson.html',
			scope : $scope,
			width : 600,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					var data = $scope.data;
					addPerson(data);
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ],
			open : function() {
				
			}
		});
	};
	
	function addPerson(data){
		$.ajax({
			url: 'alarm/addNotyPerson',
			type: 'post',
			data: data,
			dataType: 'json',
	        success: function(result){
	        	$scope.gridconfig.getGrid().grid('reload', $scope.query);
			}
		});
	}
	$scope.notycfg = notycfg.data;
	$scope.setNoty = function() {
		$http.get('alarm/getNotyCfg')
	    .success(function(data, status, headers, config){
	    // GET成功时被回调
	    	$scope.notycfg = data;
	    	box({
	    		title : '通知配置',
	    		url : 'views/alarm/notyCfg.html',
	    		scope : $scope,
	    		width : 600,
	    		drag : true,
	    		buttons : [ {
	    			text : '保存',
	    			cls : 'btn-primary',
	    			action : function() {
	    				var data = $scope.notycfg;
	    				setNoty(data);
	    				this.close();
	    			}
	    		}, {
	    			text : '关闭',
	    			cls : 'btn-default',
	    			action : function() {
	    				this.close();
	    			}
	    		
	    		} ],
	    		open : function() {
	    			
	    		}
	    	});
	    })
	    .error(function(data, status, headers, config){
	    // GET失败时被回调
	    });
	};
	
	function setNoty(data){
		$http.post('alarm/notyCfg', data).success(function(data) {

		});
	}
});