angular.module('app').controller('mainCtrl', function($scope, $state, box, $translate) {
	
	$scope.logoutUrl = 'Login/exitSystem';//引擎数据的请求路径
	
	$scope.logout = function() {
		box({
			title : '提示',
			content : '确定要退出系统吗?',
			scope : $scope,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					//loginout的请求
					$.ajax({
						url : $scope.logoutUrl,
						type : 'get',
						data : {},
						dataType : 'json',
						success : function(ret) {
							$scope.$apply(function(){
								$scope.result_loginout = ret;
								if(ret.string == "removeSession"){
									this.close();
									$state.go('login');
								}
							});
						}
					});
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ]
		});
	};
    $scope.switching = function(lang){
    	$translate.use(lang);
    };
});
