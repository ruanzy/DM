angular.module('app').controller('nodeListCtrl', function($scope, $state, box) {
   	$scope.typecfg = {
        data: [{id:'SHARD',text:'数据节点'}, {id:'ENGINE',text:'引擎节点'}, {id:'META',text:'元数据节点'}]
    };
	$scope.data = {};
	$scope.query = function(){
		$scope.gridconfig.getGrid().grid('reload', $scope.data);
	};
	$scope.gridconfig = {
   		title : '节点列表',
   		url : 'node/list',
   		pagesize : 5,
   		condition : function(){return $scope.data;},
   		columns :[
   		     {header :'节点ID', field: 'nodeId', width : 100, align : 'center'},
   		     {header :'节点类型', field: 'nodeType', render : typeRender, width : 100, align : 'center'},
   		     {header :'节点IP', field: 'ip',  width : 100, align : 'center'},
   		     {header :'操作', field: '', render : opRender, width : 100, align : 'center'}
   		]
   	};
	
	function typeRender(nodeType){
   		var val = {'SHARD':'数据节点', 'ENGINE':'引擎节点', 'META':'元数据节点'};
   		return  val[nodeType] ;
   	}
	
	function opRender(v, r){
		var nodeId = r.nodeId;
		return '<a ng-click="viewMonitorInfo(\'' + nodeId + '\')">查看</a>';
	}
	
   	$scope.viewMonitorInfo = function(nodeId){
   		box({
			title : '提示',
			url : 'login.html',
			scope : $scope,
			drag : true,
			buttons : [ {
				text : '确定',
				cls : 'btn-primary',
				action : function() {
					this.close();
				}
			}, {
				text : '关闭',
				cls : 'btn-default',
				action : function() {
					this.close();
				}

			} ]
		});
	};
});
