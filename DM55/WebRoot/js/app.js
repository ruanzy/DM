var app = angular.module('app', ['ui.router', 'oc.lazyLoad', 'pascalprecht.translate']);
app.config(["$provide","$compileProvider","$controllerProvider","$filterProvider",
	function($provide, $compileProvider, $controllerProvider, $filterProvider){
		app.controller = $controllerProvider.register;
		app.directive = $compileProvider.register;
		app.filter = $filterProvider.register;
		app.factory = $provide.factory;
		app.service =$provide.service;
		app.constant = $provide.constant;
	}
]);
app.constant("Modules_Config",[
	{
		name: 'highlight',
		module:true,
		files: [
		        'plugins/highlight/highlight.pack.js',
		        'plugins/highlight/solarized-light.css'
		]
	},
	{
		name: 'grid',
		module:true,
		files: [
		        'plugins/grid/ng-grid.js',
		        'plugins/grid/grid.css'
		]
	},
	{
		name: 'select2',
		module:true,
		files: [
		        'plugins/select2/select2.min.js',
		        'plugins/select2/ng-select2.js',
		        'plugins/select2/select2.css',
		        'plugins/select2/select2-bootstrap.css'
		]
	},
	{
		name: 'datepicker',
		module:true,
		files: [
		        'plugins/datepicker/datepicker.js',
		        'plugins/datepicker/datepicker.css'
		]
	}
])
.config(["$ocLazyLoadProvider", "Modules_Config", function($ocLazyLoadProvider, Modules_Config){	
	$ocLazyLoadProvider.config({
		debug: false,
		events: false,
		modules: Modules_Config
	});
}]);
app.config(function($translateProvider) {
//	hljsServiceProvider.setOptions({
//	    tabReplace: '    '
//	});
	$translateProvider.preferredLanguage('cn');
	$translateProvider.useStaticFilesLoader({
		prefix: 'i18n/',
		suffix: '.json'
	});
});
app.run(function($rootScope, $state, $window, $state, box) {
    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
      if (to.redirectTo) {
        evt.preventDefault();
        $state.go(to.redirectTo, params);
      }
    });
	$.ajaxSetup({
		timeout: 3000,
　　　　	//完成请求后触发。即在success或error触发后触发
　　　　	complete: function (xhr, status) { 
　　　　		if(xhr.status == 401){
　　　　			box({
　　　　				title : '提示',
    				content : '您未登录或登录失效, 请重新登录!',
    				drag : true,
    				buttons : [ 
	    				{
	    					text : '确定',
	    					cls : 'btn-primary',
	    					action : function() {
	    						$state.go('login');	    						
	    					}
	    				}, 
	    				{
	    					text : '关闭',
	    					cls : 'btn-default',
	    					action : function() {
	    						this.close();
	    					}
	
	    				} 
    				]
    			});
　　　　		}
　　　　	},
　　　　	//发送请求前触发
　　　　	beforeSend: function (xhr) {
　　　　		//可以设置自定义标头
　　　　		xhr.setRequestHeader('Authorization', $window.sessionStorage.token);
　　　　	},
	})
});

app.factory('authInterceptor', function ($rootScope, $q, $window) {
  return {
    request: function (config) {
      config.headers = config.headers || {}; 
      if ($window.sessionStorage.token) {
        config.headers.Authorization = $window.sessionStorage.token;
      }
      return config;
    },
    response: function (response) {
      if (response.status === 401) {
    	  $state.go('login');
    	  delete $window.sessionStorage.token;
      }
      return response || $q.when(response);
    }
  };
});

app.config(function ($httpProvider) {
	var param = function (obj) {
        var query = '';

        for (var name in obj) {
            var value = obj[name];

            if (value instanceof Array) {
                for (var i = 0; i < value.length; ++i) {
                    subValue = value[i];
                    var fullSubName = name + '[' + i + ']';
                    var innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value instanceof Object) {
                for (var subName in value) {
                    var subValue = value[subName];
                    var fullSubName = name + '[' + subName + ']';
                    var innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if (value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };
	$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
	$httpProvider.defaults.transformRequest = function (data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    };
	$httpProvider.interceptors.push('authInterceptor');
});
app.factory('box', function($compile, $http) {
	var box = function(opts) {
		var scope = opts.scope;
		opts.icon = 'fa-table';
		if (opts.content) {
			a(opts.content);
		} else if (opts.url) {
			$http.get(opts.url).success(function(response) {
				a(response);
			});
		}
		function a(html){
			var arr = [];
			arr.push('<div class="wrapper"><div class="dialog"><div class="modal-header bg-primary"><a type="button" class="close"><span>&#215;</span></a><h4 class="modal-title">');
			if(opts.icon){
				arr.push("<i class='fa ", opts.icon, "'></i> ");
			}
			arr.push(opts.title);
			arr.push('</h4></div><div class="modal-body">');
			arr.push(html);
			arr.push('</div>');
			if(opts.buttons){
				arr.push("<div class='modal-footer'>");
				if(opts.buttons.length > 0){
					$.each(opts.buttons,function(){	
						arr.push("<a class='btn");
						if(this.cls){
							arr.push(' ', this.cls);
						}	
						arr.push("'>");	
						arr.push(this['text']);
						arr.push('</a>');
					});	
				}else{
					arr.push("<a class='btn btn-primary'>确定</a>");
				}
				arr.push("</div>");
			}
			arr.push("</div>");
			arr.push("<div class='masker'></div>");
			arr.push("</div>");
			var template = arr.join('');
			var t = $(template);
			if(scope){
				t = $compile(template)(scope);
			}
			$('body').append(t);
			var e = t.css('zIndex', _nextZ());
			var d = t.find('.dialog');
			var header = d.find('.modal-header');
			var footer = d.find('.modal-footer');
			var btns = footer.find('.btn');
			var x = header.find('.close');
			if(opts.open){			
				opts.open.call(d);
			}
			var ww = document.documentElement.clientWidth;
			var wh = document.documentElement.clientHeight;
			d.css({
				minWidth : 350,
				maxWidth : ww,
				width : opts.width || 350,
				minHeight : 100,
				//maxHeight : wh,
				zIndex : _nextZ()
			});
			var w = d.outerWidth();
			var h = d.outerHeight();
			var l = (ww - w) / 2;
			var t = (wh - h) / 2;
			l = l < 0 ? 0 : l;
			t = t < 0 ? 0 : t;
			d.css({
				top : t,
				left : l
			});
			drag(d, header); 
			x.click(function() {
				e.remove();
			});
			footer.delegate('a', 'click', function(e) {
				var idx = btns.index(this);
				var btn = opts.buttons[idx];
				if (btn.action) {
					btn.action.call(d);
				}
			});
			d.close = function() {
				this.parent('.wrapper').hide().remove();
			};
		}
	};
	box.zIndex = 2015;
	function _nextZ() {
		return box.zIndex++;
	}
	function Median(target,min,max) {
		if (target > max) return max;
		else if (target < min) return min;
		else return target;
	}
	function drag(me, header){
		var bar = $(header, me) || me;
		bar.css({"cursor":"move"}); 
		var opts = {
			left: 0,
			top: 0,
			currentX: 0,
			currentY: 0,
			move: false
		};
    	opts.left = me.css("left");
    	opts.top = me.css("top");
		var tw = me.outerWidth();
		var th = me.outerHeight();
		$(document).mousemove(function(e) {
    		if (opts.move) {
				var nowX = e.clientX, nowY = e.clientY;
				var disX = nowX - opts.currentX, disY = nowY - opts.currentY;
				var tX = parseInt(opts.left) + disX;
				var tY = parseInt(opts.top) + disY;
				var maxw = $(document).width() - tw;
				var maxh = $(document).height() - th;
				me.css("left", Median(tX, 0, maxw) + "px");
				me.css("top", Median(tY, 0, maxh) + "px");
			}
    	});
    	$(document).mouseup(function(e) {
    		opts.move = false;	
    		opts.left = me.css("left");
    		opts.top = me.css("top");
    	});
    	bar.mousedown(function(e) {
    		opts.move = true;
    		if(!e){
    			e = window.event;
    			this.onselectstart = function(){
    				return false;
    			};  
    		}
    		opts.currentX = e.clientX;
    		opts.currentY = e.clientY;
			e.preventDefault();
    	});
	}
	return box;
});