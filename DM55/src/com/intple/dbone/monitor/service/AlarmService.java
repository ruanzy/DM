package com.intple.dbone.monitor.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.rz.common.Record;
import com.rz.dao.DB;
import com.rz.dao.RowHandler;

public class AlarmService
{
	static DB db = DB.getInstance();

	public static List<Record> getTypes()
	{
		String sql = "SELECT * FROM alarm_type";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getObjects()
	{
		String sql = "SELECT * FROM alarm_object";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getLevels()
	{
		String sql = "SELECT * FROM alarm_level";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getStrategys(Map<String, String> condition)
	{
		String type = condition.get("type");
		String level = condition.get("level");
		String object = condition.get("object");
		StringBuffer sql = new StringBuffer("SELECT * FROM alarm_strategy where 1=1");
		if (StringUtils.isNotBlank(type))
		{
			sql.append(" and type='").append(type).append("'");
		}
		if (StringUtils.isNotBlank(object))
		{
			sql.append(" and object='").append(object).append("'");
		}
		if (StringUtils.isNotBlank(level))
		{
			sql.append(" and level='").append(level).append("'");
		}
		return DB.getInstance().find(sql.toString(), new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String type = rs.getString("type");
				String object = rs.getString("object");
				String level = rs.getString("level");
				String relation = rs.getString("relation");
				int threshold = rs.getInt("threshold");
				int times = rs.getInt("times");
				String persons = rs.getString("persons");
				String notytmpl = rs.getString("notytmpl");
				r.put("id", id);
				r.put("type", type);
				r.put("object", object);
				r.put("level", level);
				r.put("relation", relation);
				r.put("threshold", threshold);
				r.put("times", times);
				r.put("persons", persons);
				r.put("notytmpl", notytmpl);
				return r;
			}
		});
	}

	public static void addStrategy(Map<String, String> params)
	{
		String sql = "INSERT INTO alarm_strategy(type, object, level, relation, threshold, times, persons, notytmpl) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
		String type = params.get("type");
		String object = params.get("object");
		String level = params.get("level");
		String relation = params.get("relation");
		int threshold = Integer.valueOf(params.get("threshold"));
		int times = Integer.valueOf(params.get("times"));
		String persons = params.get("notypersons");
		String notytmpl = params.get("notytmpl");
		DB db = DB.getInstance();
		db.begin();
		Object[] p = new Object[] { type, object, level, relation, threshold, times, persons, notytmpl };
		db.update(sql, p);
		db.commit();
	}

	public static List<Record> getNotyTypes()
	{
		String sql = "SELECT * FROM alarm_noty_type";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				r.put("id", id);
				r.put("text", name);
				return r;
			}
		});
	}

	public static List<Record> getNotyPersons()
	{
		String sql = "SELECT * FROM alarm_noty_person";
		return DB.getInstance().find(sql, new RowHandler<Record>()
		{

			@Override
			public Record handle(ResultSet rs) throws SQLException
			{
				Record r = new Record();
				String id = rs.getString("id");
				String name = rs.getString("name");
				String email = rs.getString("email");
				r.put("id", id);
				r.put("name", name);
				r.put("email", email);
				return r;
			}
		});
	}
	
	public static void addNotyPerson(Map<String, String> params)
	{
		String sql = "INSERT INTO alarm_noty_person(name, email) VALUES(?, ?)";
		String name = params.get("name");
		String email = params.get("email");
		DB db = DB.getInstance();
		db.begin();
		Object[] p = new Object[] { name, email };
		db.update(sql, p);
		db.commit();
	}
	
	public static void notyCfg(Map<String, String> params)
	{
		String sql = "update alarm_notycfg set host=?, port=?, username=?, password=?, sender=?";
		String host = params.get("host");
		String port = params.get("port");
		//String auth = params.get("auth");
		String username = params.get("username");
		String password = params.get("password");
		String sender = params.get("sender");
		DB db = DB.getInstance();
		db.begin();
		Object[] p = new Object[] { host, port, username, password, sender };
		db.update(sql, p);
		db.commit();
	}
	
	public static Record getNotyCfg()
	{
		String sql = "SELECT * FROM alarm_notycfg";
		return DB.getInstance().findOne(sql);
	}

	public static Record getStrategyNoty(String id) {
		String sql = "SELECT persons, notytmpl FROM alarm_strategy where id = ?";
		return DB.getInstance().findOne(sql, new Object[] { id });
	}

	public static void setStrategyNoty(Map<String, String> params) {
		String sql = "update alarm_strategy set persons=?, notytmpl=? where id=?";
		String persons = params.get("persons");
		String notytmpl = params.get("tmpl");
		String id = params.get("id");
		DB db = DB.getInstance();
		db.begin();
		Object[] p = new Object[] { persons, notytmpl, id};
		db.update(sql, p);
		db.commit();
	}
}
