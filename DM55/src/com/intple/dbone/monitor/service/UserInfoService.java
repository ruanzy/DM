package com.intple.dbone.monitor.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.rz.common.Record;
import com.rz.dao.DB;
import com.rz.dao.RowHandler;

public class UserInfoService {
	static DB db = DB.getInstance();
	
	public static List<Record> getUserByName(final String _username, final String _password){
		String sql = "SELECT * FROM userinfo where username ='" + _username + "'";
		return DB.getInstance().find(sql, new RowHandler<Record>(){

			@Override
			public Record handle(ResultSet rs) throws SQLException{
				Record r = new Record();
				String username = rs.getString("username");
				String password = rs.getString("password");
				r.put("username", username);
				r.put("password", password);
				
				if(r == null || "".equals(r)){
					return null;
				}else{
					if(password.equals(_password)){
						return r;
		    		}else{
		    			return null;
		    		}
				}
			}
		});
	}
}
