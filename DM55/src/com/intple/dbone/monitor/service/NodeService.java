package com.intple.dbone.monitor.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.intple.dbone.monitor.vo.NodeVO;
import com.intple.dbone.monitor.vo.NodeVO.NodeType;
import com.rz.dao.DB;
import com.rz.dao.RowHandler;

public class NodeService {
	static DB dbone = DB.getDboneInstance();

	/**
	 * 从engine获取节点
	 * 
	 * @return
	 */
	public static List<NodeVO> getNodesFromEngine() {
		List<NodeVO> list = dbone.find("config@show node",
				new RowHandler<NodeVO>() {
					public NodeVO handle(ResultSet rs) throws SQLException {
						NodeVO node = new NodeVO();
						int columns = rs.getMetaData().getColumnCount();
						node.setNodeId(rs.getString(1));
						node.setIp(rs.getString(2));
						node.setHeartip(rs.getString(3));
						node.setUsername(rs.getString(4));
						node.setPassword(rs.getString(5));
						node.setSshPort(rs.getInt(6));
						if (columns == 9) {
							node.setAppNodeServPort(rs.getInt(7));
							node.setIdbdir(rs.getString(8));
							node.setWeight(rs.getInt(9));
						} else {
							node.setIdbdir(rs.getString(7));
							node.setWeight(rs.getInt(8));
						}
						node.setNodeType(NodeType.SHARD);
						return node;
					}
				});
		return list;
	}

	/**
	 * 从engine获取engine结群节点
	 * 
	 * @return
	 */
	public static List<NodeVO> getEngineNodes() {
		List<NodeVO> list = dbone.find("config@monitor queryserver",
				new RowHandler<NodeVO>() {
					public NodeVO handle(ResultSet rs) throws SQLException {
						NodeVO node = new NodeVO();
						String ip = rs.getString(1);
						node.setNodeId("engine_" + ip);
						node.setIp(ip);
						node.setNodeType(NodeType.ENGINE);
						return node;
					}
				});
		return list;
	}

	/**
	 * 获取元数据节点
	 * 
	 * @return
	 */
	public static List<NodeVO> getMetadataNodes() {
		List<NodeVO> list = dbone.find("config@show metadatanode",
				new RowHandler<NodeVO>() {
					public NodeVO handle(ResultSet rs) throws SQLException {
						NodeVO node = new NodeVO();
						node.setNodeId(rs.getString(1));
						node.setIp(rs.getString(2));
						node.setNodeType(NodeType.META);
						return node;
					}

				});
		return list;
	}

	public static List<Map<String, String>> getCpuInfo(
			List<Map<String, String>> nodes) {
		List<Map<String, String>> nodesCpuInfo = new ArrayList<Map<String, String>>();
		return nodesCpuInfo;

	}

	public static List<Map<String, String>> getMemoryInfo(
			List<Map<String, String>> nodes) {
		List<Map<String, String>> nodesMemoryInfo = new ArrayList<Map<String, String>>();
		return nodesMemoryInfo;

	}

	public static List<Map<String, String>> getDiskInfo(
			List<Map<String, String>> nodes) {
		List<Map<String, String>> nodesDiskInfo = new ArrayList<Map<String, String>>();
		return nodesDiskInfo;

	}

	public static List<Map<String, String>> getNetworkInfo(
			List<Map<String, String>> nodes) {
		List<Map<String, String>> nodesNetworkInfo = new ArrayList<Map<String, String>>();
		return nodesNetworkInfo;

	}
}
