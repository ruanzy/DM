package com.intple.dbone.monitor.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.intple.dbone.monitor.common.Node;

public class AppNodesService {
	
public List<Map<String,String>> retrieveDataFromAppNodes() {
		
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		//todo
		Node node1 = new Node();
		node1.setIp("11.0.0.31");
		node1.setRootPassword("!qaz2wsx");
		node1.setSshPort(22);
		node1.setUsername("root");
		node1.setPassword("!qaz2wsx");
		List<Node> nodeList = new ArrayList<Node>();
		nodeList.add(node1);	
		ExecutorService executorPool = Executors.newCachedThreadPool();
		CompletionService<AppNodeService.SSHContext> completionService = new ExecutorCompletionService<AppNodeService.SSHContext>(
				executorPool);
		for (final Node vo : nodeList) {
			Callable<AppNodeService.SSHContext> callable = new Callable<AppNodeService.SSHContext>() {
				@Override
				public AppNodeService.SSHContext call() throws Exception {
					return new AppNodeService(vo).execute();
				}
			};
			completionService.submit(callable);
		}
		
		AppNodeService.SSHContext context;
		for (int i = 0; i < nodeList.size(); i++) {
			try {
				context = completionService.take().get();
				result.add(context.capability);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;	
	}

}
