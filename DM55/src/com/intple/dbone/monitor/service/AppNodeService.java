package com.intple.dbone.monitor.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intple.dbone.monitor.common.DBOneMonitorException;
import com.intple.dbone.monitor.common.IOUtils;
import com.intple.dbone.monitor.common.Node;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.UserInfo;


public class AppNodeService {
	static final Logger logger = LoggerFactory.getLogger(AppNodeService.class);
	
	private SSHConnectionPool sshConnectionPool = new SSHConnectionPool();
	
	private Node node;
	
	public AppNodeService(Node node) {
		this.node = node;
	}
	
	public class SSHContext {
		public SSHConnection conn;
		public Node node;
		public String scriptPath;
		public Map<String,String> capability;
	}

	private interface SSHExecutor {
		public SSHContext execute(SSHContext context) throws Exception;
	}
	
	private class AppnodeParser {
		private String result;
		private SSHContext context;
		public AppnodeParser(String result,SSHContext context){
			this.result = result;
			this.context = context;
		}
		public void parse(){
			for (String entry : result.split("\n")){
				context.capability.put(entry.split("=")[0],entry.split("=")[1]);
			}
		}
	}
	
	
	
	private class SSHConnectionPool {

		private Map<String, SSHConnection> map = new HashMap<String, SSHConnection>();

		private Map<String, Integer> set = new HashMap<String, Integer>();

		private ReentrantLock lock = new ReentrantLock();

		private int faildCount = 3; // 尝试失败3次后，强制关闭连接

		public SSHConnection connect(String host, int port, String user,
				String password) throws Exception {
			try {
				lock.lock();
				String key = host + ":" + port;
				SSHConnection c = map.get(key);
				if (set.containsKey(key)) {
					set.put(key, set.get(key) + 1);
					if (set.get(key) >= faildCount) {
						this.close(host, port);
					}
					return null;
				}
				if (c == null) {
					c = new SSHConnection(host, port, user, password);
					map.put(key, c);
				}
				set.put(key, 0);
				return c;
			} finally {
				lock.unlock();
			}
		}

		public void close(String host, int port) {
			try {
				lock.lock();
				String key = host + ":" + port;
				SSHConnection c = map.get(key);
				if (c != null)
					c.close();
				map.remove(key);
				set.remove(key);
			} finally {
				lock.unlock();
			}
		}

		@SuppressWarnings("unused")
		public void closeAll() {
			try {
				lock.lock();
				if (map.size() > 0) {
					for (SSHConnection c : map.values())
						c.close();
					map.clear();
				}
				set.clear();
			} finally {
				lock.unlock();
			}
		}

		public void release(String host, int port) {
			try {
				lock.lock();
				set.remove(host + ":" + port);
			} finally {
				lock.unlock();
			}
		}
	}
	
	private class SSHConnection {
		private com.jcraft.jsch.Session session = null;
		private int sshTimeout = 5000;

		public SSHConnection(String host, int port, String user, String password)
				throws Exception {
			init(host, port, user, password);
		}

		private void init(String host, int port, String user, String password)
				throws Exception {
			try {
				JSch jSch = new JSch();
				session = jSch.getSession(user, host, port);
				SshUserInfo userInfo = new SshUserInfo();
				userInfo.setPassword(password);
				session.setUserInfo(userInfo);
				session.setPassword(password);
				session.setTimeout(sshTimeout);
				session.connect();
			} catch (JSchException e) {
				throw new DBOneMonitorException("error connect to : " + host + ","
						+ port + "," + user + "," + password);
			}
		}

		public String execute(String cmd) throws Exception {
			Channel channel = null;
			try {
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(". /etc/profile;" + cmd);
				channel.setInputStream(null);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				((ChannelExec) channel).setErrStream(baos);
				InputStream is = channel.getInputStream();
				channel.connect();
				String result = IOUtils.toString(is);
				is.close();
				String errMsg = baos.toString();
				if (errMsg.length() > 0
						&& !errMsg.startsWith("cygwin warning:")) {
					throw new DBOneMonitorException(errMsg);
				}
				return result;
			} catch (JSchException e) {
				throw new DBOneMonitorException("error executing : " + cmd);
			} catch (IOException e) {
				throw new DBOneMonitorException("error executing : " + cmd);
			} finally {
				try {
					channel.disconnect();
				} catch (Exception e) {
				}
			}

		}

		public void close() {
			try {
				session.disconnect();
			} catch (Exception e) {
			}
		}
	}
	
	private class SshUserInfo implements UserInfo {
	    
	    private boolean firstTime;
	    private String password;

	    @Override
	    public void showMessage(String message) {
	        logger.info(message);
	    }

	    @Override
	    public boolean promptYesNo(String message) {
	        // trust all hosts
	        return true;
	    }

	    @Override
	    public boolean promptPassword(String message) {
	        if (firstTime) {
	            firstTime = false;
	            return true;
	        }
	        return firstTime;
	    }

	    @Override
	    public boolean promptPassphrase(String message) {
	        return true;
	    }

	    @Override
	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

	    @Override
	    public String getPassphrase() {
	        return null;
	    }
	}
	
	
	
	private SSHExecutor executor = new SSHExecutor() {

		@Override
		public SSHContext execute(SSHContext context) throws Exception {
			String command="/bin/sh " + context.scriptPath;
			String result = context.conn.execute(command);
			if(result !=null){
				new AppnodeParser(result,context).parse();
				return context;
			}
			return null;
		}
	};
	
	@SuppressWarnings("deprecation")
	public SSHContext execute() {
		String ip = node.getHeartip();
		if (ip == null || ip.equals("")) {
			ip = node.getIp();
		}
		SSHContext context = new SSHContext();
		context.capability = new HashMap<String,String>();
		context.capability.put("ip", ip);
		context.capability.put("time",(new Date().toLocaleString()));
		context.node = node;
		context.scriptPath = "/appnode_monitor.sh";
		try {
			context.conn = sshConnectionPool.connect(node.getIp(),
					node.getSshPort(), node.getUsername(),
					node.getPassword());
			if (context.conn == null) {
				logger.debug("skipping " + ip + " system info :"  + Thread.currentThread());
				return null;
			}
			executor.execute(context);
			sshConnectionPool.release(node.getIp(), node.getSshPort());
			return context;
		} catch (Exception e) {
			sshConnectionPool.close(node.getIp(), node.getSshPort());
			return null;
		}

	}
}
