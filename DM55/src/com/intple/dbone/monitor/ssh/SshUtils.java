package com.intple.dbone.monitor.ssh;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intple.dbone.monitor.common.DboneMonitorRuntimeException;
import com.intple.dbone.monitor.vo.NodeVO;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;

public class SshUtils {

	static final Logger logger = LoggerFactory.getLogger(SshUtils.class);
	private static int sshTimeout = 5000;

	public static class SSHConnection {
		private com.jcraft.jsch.Session session = null;
		private com.jcraft.jsch.Channel channel = null;

		public SSHConnection(String host, int port, String user, String password)
				throws DboneMonitorRuntimeException {
			init(host, port, user, password);
		}

		private void init(String host, int port, String user, String password)
				throws DboneMonitorRuntimeException {
			try {
				JSch jSch = new JSch();
				session = jSch.getSession(user, host, port);
				SshUserInfo userInfo = new SshUserInfo();
				userInfo.setPassword(password);
				session.setUserInfo(userInfo);
				session.setPassword(password);
				session.setTimeout(sshTimeout);
				session.connect();
			} catch (JSchException e) {
				throw new DboneMonitorRuntimeException("error connect to : " + host
						+ "," + port + "," + user + "," + password);
			}
		}

		public String execute(String cmd) {
			try {
				channel = session.openChannel("exec");
				((ChannelExec) channel).setCommand(". /etc/profile;" + cmd);
				channel.setInputStream(null);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				((ChannelExec) channel).setErrStream(baos);
				InputStream is = channel.getInputStream();
				channel.connect();
				String result = IOUtils.toString(is);
				is.close();
				String errMsg = baos.toString();
				if (errMsg.length() > 0
						&& !errMsg.startsWith("cygwin warning:")) {
					logger.error(errMsg);
					throw new DboneMonitorRuntimeException(errMsg);
				}
				return result;
			} catch (Exception e) {
				if (e instanceof DboneMonitorRuntimeException) {
					throw (DboneMonitorRuntimeException) e;
				}
				throw new DboneMonitorRuntimeException("error executing : " + cmd);
			}
		}

		public void close() {
			try {
				if (channel != null) {
					channel.disconnect();
					channel = null;
				}
			} catch (Exception e) {
			}
			try {
				if (session != null) {
					session.disconnect();
					session = null;
				}
			} catch (Exception e) {
			}
		}
	}

	public static class SSHContext {
		public SSHConnection conn;
		public NodeVO node;
	}

	public static String exec(String host, int port, String username,
			String password, String cmd) {
		String res = null;
		SSHConnection sshConn = null;
		try {
			sshConn = new SSHConnection(host, port, username, password);
			res = sshConn.execute(cmd);
		} catch (DboneMonitorRuntimeException e) {
			logger.error("execute command fail : host = " + host + ", port = "
					+ port + ", username = " + username + ", password = "
					+ password + ", cmd = " + cmd + ", Err:" + e.getMessage());
			throw e;
		} finally {
			sshConn.close();
		}
		return res;
	}

	public static String exec(NodeVO node, String cmd) {
		return exec(node.getIp(), node.getSshPort(), node.getUsername(),
				node.getPassword(), cmd);
	}

	public static void exec(String host, int port, String username,
			String password, SSHExecutor[] capabilityExecutors) {
		SSHContext context = new SSHContext();
		SSHConnection sshConn = null;
		try {
			NodeVO node = new NodeVO();
			node.setIp(host);
			node.setSshPort(port);
			node.setUsername(username);
			node.setPassword(password);
			context.node = node;
			sshConn = new SSHConnection(host, port, username, password);
			context.conn = sshConn;
			for (SSHExecutor ex : capabilityExecutors) {
				context = ex.execute(context);
			}
		} catch (Exception e) {
			logger.error("execute command fail : host = " + host + ", port = "
					+ port + ", username = " + username + ", password = "
					+ password + ", Err:" + e.getMessage());
			throw new DboneMonitorRuntimeException(e);
		} finally {
			sshConn.close();
		}
	}

	public static void exec(NodeVO node, SSHExecutor[] capabilityExecutors) {
		SSHContext context = new SSHContext();
		String host = node.getIp();
		int port = node.getSshPort();
		String username = node.getUsername();
		String password = node.getPassword();
		SSHConnection sshConn = null;
		try {
			sshConn = new SSHConnection(node.getIp(), node.getSshPort(),
					username, password);
			context.node = node;
			context.conn = sshConn;
			for (SSHExecutor ex : capabilityExecutors) {
				context = ex.execute(context);
			}
		} catch (Exception e) {
			logger.error("execute command fail : host = " + host + ", port = "
					+ port + ", username = " + username + ", password = "
					+ password + ", Err:" + e.getMessage());
			throw new DboneMonitorRuntimeException(e);
		} finally {
			sshConn.close();
		}
	}

}
