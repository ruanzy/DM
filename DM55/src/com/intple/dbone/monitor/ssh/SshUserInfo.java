package com.intple.dbone.monitor.ssh;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.UserInfo;

public class SshUserInfo implements UserInfo {
    private static final Logger logger = LoggerFactory.getLogger(SshUserInfo.class);

    private boolean firstTime;
    private String password;

    @Override
    public void showMessage(String message) {
        logger.info(message);
    }

    @Override
    public boolean promptYesNo(String message) {
        // trust all hosts
        return true;
    }

    @Override
    public boolean promptPassword(String message) {
        if (firstTime) {
            firstTime = false;
            return true;
        }
        return firstTime;
    }

    @Override
    public boolean promptPassphrase(String message) {
        return true;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getPassphrase() {
        return null;
    }
}