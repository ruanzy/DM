package com.intple.dbone.monitor.ssh;

import com.intple.dbone.monitor.ssh.SshUtils.SSHContext;

public interface SSHExecutor {
	public SSHContext execute(SSHContext context) throws Exception;
}
