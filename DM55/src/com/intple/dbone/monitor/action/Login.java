package com.intple.dbone.monitor.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.intple.dbone.monitor.service.UserInfoService;
import com.rz.common.Base64Util;
import com.rz.common.Record;
import com.rz.common.TokenUtil;
import com.rz.web.Json;
import com.rz.web.View;
import com.rz.web.WebUtil;
import com.rz.web.WebUtil.Request;

public class Login {

	//用户登录
	public View userLogin(){
		String _username = WebUtil.getParameter("username");
		String _password = WebUtil.getParameter("password");
		_password = Base64Util.decode(_password);
		List<Record> list = UserInfoService.getUserByName(_username, _password);

		HttpServletRequest request = Request.get();
		
		//用于返回错误信息
		Map<String, Object> ret = new HashMap<String, Object>();
		
    	if(list.size() == 0 || list == null){
    		ret.put("error", "unknowusername");
			return new Json(ret);
    	}else{
    		if(_password.equals(list.get(0).get("password")) == false){
    			ret.put("error", "errorpassword");
    			return new Json(ret);
    		}else{
    			//WebUtil.Session.attr("userInfo", list);//把adminInfo信息存入session
    			String token = TokenUtil.generatorToken(_username);
    			request.getSession().setAttribute("userInfo", list);//把adminInfo信息存入session
    			ret.put("token", token);
    			return new Json(ret);
    		}
    	}
	}
	
	//退出
	public View exitSystem(){
		//用于返回错误信息
		Map<String, Object> ret = new HashMap<String, Object>();
		HttpServletRequest request = Request.get();
		request.getSession().removeAttribute("userInfo");
		//WebUtil.Session.clear();
		ret.put("string", "removeSession");
		return new Json(ret);
	}
}
