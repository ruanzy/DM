package com.intple.dbone.monitor.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.intple.dbone.monitor.service.NodeService;
import com.intple.dbone.monitor.vo.NodeVO;
import com.intple.dbone.monitor.vo.NodeVO.NodeType;
import com.rz.web.Json;
import com.rz.web.View;
import com.rz.web.WebUtil;

public class Node {
	public View list() {
		String appName = WebUtil.getParameter("appName");
		System.out.println(appName);
		List<NodeVO> result = new ArrayList<NodeVO>();
		String type = WebUtil.getParameter("type");
		if (StringUtils.equals(type, "SHARD") || StringUtils.isBlank(type)){
			if ("app1".equals(appName)) {
				NodeVO node1 = new NodeVO();
				node1.setNodeId("11.0.1.85");
				node1.setIp("11.0.1.85");
				node1.setNodeType(NodeType.SHARD);
				NodeVO node2 = new NodeVO();
				node2.setNodeId("11.0.1.84");
				node2.setIp("11.0.1.84");
				node2.setNodeType(NodeType.SHARD);
				result.add(node1);
				result.add(node2);
			} else {
				NodeVO node1 = new NodeVO();
				node1.setNodeId("11.0.1.87");
				node1.setIp("11.0.1.87");
				node1.setNodeType(NodeType.SHARD);
				NodeVO node2 = new NodeVO();
				node2.setNodeId("11.0.1.88");
				node2.setIp("11.0.1.88");
				node2.setNodeType(NodeType.SHARD);
				result.add(node1);
				result.add(node2);
			}
			List<NodeVO> shardNode = NodeService.getNodesFromEngine();
			result.addAll(shardNode);
		}
		if (StringUtils.equals(type, "META") || StringUtils.isBlank(type)) {
			List<NodeVO> metadataNode = NodeService.getMetadataNodes();
			result.addAll(metadataNode);
		}
		if (StringUtils.equals(type, "ENGINE") || StringUtils.isBlank(type)) {
			List<NodeVO> engineNode = NodeService.getEngineNodes();
			result.addAll(engineNode);
		}
		String page = WebUtil.getParameter("page");
		String pagesize = WebUtil.getParameter("pagesize");
		if (StringUtils.isNotBlank(page) && StringUtils.isNotBlank(pagesize)) {
			int p = Integer.valueOf(page);
			int ps = Integer.valueOf(pagesize);
			Map<String, Object> ret = new HashMap<String, Object>();
			int total = result.size();
			int begin = (p - 1) * ps;
			int end = p * ps;
			if (end >= total) {
				end = total;
			}
			List<NodeVO> pageList = new ArrayList<NodeVO>();
			for (int i = begin; i < end; i++) {
				pageList.add(result.get(i));
			}
			ret.put("total", total);
			ret.put("data", pageList);
			ret.put("page", page);
			ret.put("pagesize", pagesize);
			return new Json(ret);
		}
		return new Json(result);
	}

	public View baseinfo() {
		// String ip = WebUtil.getParameter("ip");
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("ip", "11.0.1.85");
		m.put("pid", "9201");
		m.put("osName", "Linux");
		return new Json(m);
	}

}
