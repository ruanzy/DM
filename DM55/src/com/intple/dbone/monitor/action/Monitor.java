package com.intple.dbone.monitor.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.intple.dbone.monitor.service.AppNodesService;
import com.rz.web.Html;
import com.rz.web.Json;
import com.rz.web.View;

public class Monitor
{
	public View execute()
	{
		return new Html("monitor.html");
	}

	public View nodes()
	{
		List<String> result = new ArrayList<String>();
		result.add("11.0.1.85");
		result.add("11.0.1.84");
		return new Json(result);
	}
	
	public View node()
	{
		//Map<String, Map<String, Object>> result = NodesMonitor.getInstance().getData();
		return new Json(null);
	}

	public View engine()
	{
		//Map<String, Object> result = EngineMonitor.getInstance().getData();
		return new Json(null);
	}
	
	public View appNodes()
	{
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		boolean mock = true;
		if(mock){
			Map<String,String> map1= new HashMap<String,String>();
			map1.put("AppServer", "11.0.0.31");
			map1.put("AppServerPort", "29000");
			map1.put("cpuUsage", "10");
			Map<String,String> map2= new HashMap<String,String>();
			map2.put("AppServer", "11.0.0.32");
			map2.put("AppServerPort", "29000");
			map2.put("cpuUsage", "13");
			result.add(map1);
			result.add(map2);
		}else{
			result = new AppNodesService().retrieveDataFromAppNodes();
		}
		return new Json(result);
	}
	

	
	
	public static void main(String[] args) throws Exception
	{
		Monitor monitor = new Monitor();
		monitor.appNodes();
	}
	
}
