package com.intple.dbone.monitor.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rz.web.Html;
import com.rz.web.Json;
import com.rz.web.View;

public class Engine
{
	public View execute()
	{
		//这个是默认执行的方法
		return new Html("engine.html");
	}

	public View list()
	{
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("ip", "11.0.1.85");
		m1.put("state", "1");
		
		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("ip", "11.0.1.84");
		m2.put("state", "2");
		
		Map<String, Object> m3 = new HashMap<String, Object>();
		m3.put("ip", "11.0.1.86");
		m3.put("state", "3");
		
		result.add(m1);
		result.add(m2);
		result.add(m3);
		return new Json(result);
	}
	
	public View baseinfo(){
		//String ip = WebUtil.getParameter("ip");
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("ip", "11.0.1.85");
		m.put("pid", "9201");
		m.put("osName", "Linux");
		return new Json(m);
	}
}
