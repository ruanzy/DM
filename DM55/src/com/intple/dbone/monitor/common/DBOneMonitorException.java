package com.intple.dbone.monitor.common;

public class DBOneMonitorException extends Exception {
	private static final long serialVersionUID = -1186158470435067520L;

	public DBOneMonitorException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DBOneMonitorException(String message, Throwable cause) {
		super(message, cause);
	}

	public DBOneMonitorException(String message) {
		super(message);
	}

	public DBOneMonitorException(Throwable cause) {
		super(cause);
	}
}
