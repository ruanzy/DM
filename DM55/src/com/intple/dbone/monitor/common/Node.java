package com.intple.dbone.monitor.common;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class Node implements Serializable {
	private static final long serialVersionUID = 8196888390924863563L;

	private Integer id;
	private String nodeId;
	private String ip;
	private String heartip;
	private String username;
	private String password;
	private int sshPort;
	private int appNodeServPort;
	private String idbdir;
	private boolean status;
	private int weight;
	
	private String lic;
	private String rootPassword;
	private boolean idbinstalled;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getHeartip() {
		return heartip;
	}

	public void setHeartip(String heartip) {
		this.heartip = heartip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getSshPort() {
		return sshPort;
	}
	
	public void setSshPort(int sshPort) {
		this.sshPort = sshPort;
	}
	
	public String getIdbdir() {
		return idbdir;
	}
	
	public void setIdbdir(String idbdir) {
		this.idbdir = idbdir;
	}

	public int getAppNodeServPort() {
		return appNodeServPort;
	}

	public void setAppNodeServPort(int appNodeServPort) {
		this.appNodeServPort = appNodeServPort;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getLic() {
		return lic;
	}

	public void setLic(String lic) {
		if(StringUtils.isNotBlank(lic)){
			this.lic = lic.replace("\n", "\\n");
		}
	}

	@Deprecated
	public String getRootPassword() {
		return rootPassword;
	}

	public void setRootPassword(String rootPassword) {
		this.rootPassword = rootPassword;
	}
	
	public boolean getIdbinstalled() {
		return idbinstalled;
	}

	public void setIdbinstalled(boolean idbinstalled) {
		this.idbinstalled = idbinstalled;
	}
	
}
