package com.intple.dbone.monitor.common;


public class DboneMonitorRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 5565773570357425148L;

	public DboneMonitorRuntimeException() {
		super();
	}
	
	public DboneMonitorRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public DboneMonitorRuntimeException(String message) {
		super(message);
	}

	public DboneMonitorRuntimeException(Throwable cause) {
		super(cause);
	}

	public DboneMonitorRuntimeException(Exception exception) {
		super(exception);
	}
}
