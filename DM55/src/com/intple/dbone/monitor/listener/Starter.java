package com.intple.dbone.monitor.listener;

import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rz.dao.DB;

public class Starter implements ServletContextListener {
	static final Logger logger = LoggerFactory.getLogger(Starter.class);

	public void contextDestroyed(ServletContextEvent e) {

	}

	public void contextInitialized(ServletContextEvent e) {
		logger.info("系统初始化开始...");
		String table = "alarm_strategy_noty";
		DB db = DB.getInstance();
		String sql = "SELECT COUNT(*) FROM sqlite_master where type='table' and name=?";
		Object obj = db.scalar(sql, new Object[] { table });
		boolean isexsist = (obj != null)
				&& (Integer.valueOf(obj.toString()) > 0);
		if (!isexsist) {
			logger.info("执行dbonemonitor.sql...");
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("dbonemonitor.sql");
			db.runScript(new InputStreamReader(is));
		}
		logger.info("系统初始化结束...");
	}
}
