package com.intple.dbone.monitor.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intple.dbone.monitor.support.SchedulerUtil;

public class SchedulerStarter implements ServletContextListener {
	static final Logger logger = LoggerFactory.getLogger(SchedulerStarter.class);

	public void contextDestroyed(ServletContextEvent e) {
		try
		{
			SchedulerUtil.shutdown();
		}catch(Exception e1)
		{
			logger.error(e1.getMessage());
		}		

	}

	public void contextInitialized(ServletContextEvent e) {
		try
		{
			SchedulerUtil.start();
		}catch(Exception e1)
		{
			logger.error(e1.getMessage());
		}		
	}
}
