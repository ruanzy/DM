package com.intple.dbone.monitor;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.nio.SelectChannelConnector;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBOneMonitor
{
	static final Logger logger = LoggerFactory.getLogger(DBOneMonitor.class);

	public static void main(String[] args) throws Exception
	{
		String WebRoot = "WebRoot";
		Server server = new Server();
		Connector connector = new SelectChannelConnector();
		connector.setPort(8088);
		server.setConnectors(new Connector[] { connector });
		WebAppContext webAppContext = new WebAppContext(WebRoot, "");
		webAppContext.setDescriptor(WebRoot + "/WEB-INF/web.xml");
		webAppContext.setResourceBase(WebRoot);
		webAppContext.setDisplayName("DBOneMonitor");
		webAppContext.setClassLoader(Thread.currentThread().getContextClassLoader());
		webAppContext.setConfigurationDiscovered(true);
		webAppContext.setParentLoaderPriority(true);
		server.setHandler(webAppContext);
		try
		{
			server.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		logger.info("server is  started in port 8088");
	}
}
