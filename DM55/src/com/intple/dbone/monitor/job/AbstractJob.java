package com.intple.dbone.monitor.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/*
 * 继承Job接口表示无状态任务，该抽象类应
 * 包含更多的上下文获取的方法。
 */
public class AbstractJob implements Job {
	
	//getConfigContext();
	
	//getRRDStore();

	@Override
	public void execute(JobExecutionContext execContext) throws JobExecutionException {

	}

}
