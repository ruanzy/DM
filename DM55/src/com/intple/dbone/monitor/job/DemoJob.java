package com.intple.dbone.monitor.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.impl.JobDetailImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DemoJob extends AbstractJob {
	 static Logger logger = LoggerFactory.getLogger(DemoJob.class);   
	  
     public void execute(JobExecutionContext context)   
               throws JobExecutionException {   
  
          // Every job has its own job detail   
    	 JobDetailImpl jobDetail = (JobDetailImpl)context.getJobDetail();   
  
          // The name and group are defined in the job detail   
          String jobName = jobDetail.getName();  
          logger.info("Name: " + jobDetail.getFullName());   
          
          //获取调度是传入的数据信息；
          //jobDetail.getJobDataMap();
  
          // The name of this class configured for the job   
          logger.info("Job Class: " + jobDetail.getJobClass());   
  
          // Log the time the job started   
          logger.info(jobName + " fired at " + context.getFireTime());   
  
         logger.info("Next fire time " + context.getNextFireTime());   
     }   


}
