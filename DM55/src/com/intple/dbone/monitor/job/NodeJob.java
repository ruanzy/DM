package com.intple.dbone.monitor.job;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.intple.dbone.monitor.ssh.SSHExecutor;
import com.intple.dbone.monitor.ssh.SshUtils;
import com.intple.dbone.monitor.ssh.SshUtils.SSHContext;
import com.intple.dbone.monitor.support.SchedulerUtil;
import com.intple.dbone.monitor.vo.NodeVO;

public class NodeJob implements Job {
	static final Logger logger = LoggerFactory.getLogger(NodeJob.class);
	private final static String cron = "0/15 * * * * ?";

	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		List<NodeVO> nodes = (List<NodeVO>) dataMap.get("nodes");
		SSHExecutor[] capabilityExecutors = { new SSHExecutor() {
			public SSHContext execute(SSHContext context) throws Exception {
				String systemVersion = context.conn
						.execute("v1=`uname -o` && v2=`lsb_release -d` && echo $v1${v2:12}");
				logger.debug(context.node + " " + systemVersion);
				return context;
			}
		} };
		for (NodeVO node : nodes) {
			SshUtils.exec(node, capabilityExecutors);
		}
	}

	public static void addJob(List<NodeVO> nodes) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("nodes", nodes);
		SchedulerUtil.addTask(NodeJob.class, cron, data);
	}
}
