package com.intple.dbone.monitor.support.mail;
public class SimpleMail {
	private String content;
	private String subject;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public SimpleMail(String content, String subject) {
		super();
		this.content = content;
		this.subject = subject;
	}
	
	
	
	
}
