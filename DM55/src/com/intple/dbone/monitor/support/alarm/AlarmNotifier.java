package com.intple.dbone.monitor.support.alarm;

public interface AlarmNotifier {
	public void process() throws Exception;
}
