package com.intple.dbone.monitor.support.alarm;

import java.util.ArrayList;
import java.util.List;


public class MultiAlarmNotifier implements AlarmNotifier {
	
	List<AlarmNotifier>  alarmNotifers=new ArrayList<AlarmNotifier>();
	
	public void register(AlarmNotifier an)
	{
		alarmNotifers.add(an);
	}
	
    
	//TODO  配置上下文对象的获取和信息;
	@Override
	public void process()  throws Exception {
		List<Exception> exceptions=new ArrayList<Exception>();
		for(AlarmNotifier an:alarmNotifers)
		{
			try
			{
			   an.process();
			}catch(Exception e)
			{
				exceptions.add(e);
			}			
		}
		if(exceptions.size()>0)
		{
			throw new RuntimeException();
		}
	}
}
