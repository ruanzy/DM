package com.intple.dbone.monitor.support.alarm;

import com.intple.dbone.monitor.support.mail.SimpleMail;
import com.intple.dbone.monitor.support.mail.SimpleMailSender;


public class MailAlarmNotifier implements AlarmNotifier {
	private String smtpServer;
	private String userMail;
	private String userPassword;
	private String targetMail;
	private String subject;
	private String content;
	
	
	public MailAlarmNotifier()
	{
		
	}
	
	public MailAlarmNotifier(String smtpServer, String userMail,String userPassword,
			String targetMail,String subject,String content)
	{
		this.smtpServer=smtpServer;
		this.userMail=userMail;
		this.userPassword=userPassword;
		this.targetMail=targetMail;
		this.subject=subject;
		this.content=content;
	}
    
	//TODO  配置上下文对象的获取和信息;
	@Override
	public void process()  throws Exception {
		//从整体上下文获取配置信息；
		//smtp服务器
		//用户名/密码
		//收件人.主题以及内容
		SimpleMailSender sender = new SimpleMailSender(this.smtpServer, 
				this.userMail, this.userPassword); 
		sender.send(this.targetMail, new SimpleMail(this.subject, this.content));		  
	      
	    }  
}
