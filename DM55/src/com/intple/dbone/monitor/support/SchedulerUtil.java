package com.intple.dbone.monitor.support;

import java.text.ParseException;
import java.util.Map;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerUtil {
	private static Logger logger = LoggerFactory.getLogger(SchedulerUtil.class);

	private static Scheduler scheduler;

	private static void init() throws SchedulerException {
		if (scheduler == null) {
			scheduler = StdSchedulerFactory.getDefaultScheduler();

		}
	}

	public static Scheduler getInstance() throws SchedulerException {
		if (scheduler == null) {
			init();

		}
		return scheduler;
	}

	public static void start() throws SchedulerException {
		if (scheduler == null) {
			scheduler = StdSchedulerFactory.getDefaultScheduler();

		}
		scheduler.start();
		if (logger.isInfoEnabled())
			logger.info("scheduler started ....");

	}

	public static void shutdown() throws SchedulerException {
		scheduler.shutdown(true);
		if (logger.isInfoEnabled())
			logger.info("scheduler started ....");

	}

	public static void addTask(Class<? extends Job> jobClass, String cron,
			Map<String, Object> data) {
		JobDetail jobDetail = JobBuilder.newJob(jobClass).build();
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			jobDetail.getJobDataMap().put(entry.getKey(), entry.getValue());
		}
		CronScheduleBuilder builder = null;
		try {
			builder = CronScheduleBuilder.cronSchedule(cron);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Trigger trigger = TriggerBuilder.newTrigger().startNow()
				.withSchedule(builder).build();
		try {
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {

		}
	}

}
