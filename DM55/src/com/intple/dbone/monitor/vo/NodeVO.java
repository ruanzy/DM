package com.intple.dbone.monitor.vo;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class NodeVO implements Serializable {

	private static final long serialVersionUID = -440135844101456447L;

	public enum NodeType {
		SHARD("数据节点"), ENGINE("引擎节点"), META("元数据节点");
		public final String name;

		NodeType() {
			this(null);
		}

		NodeType(String name) {
			this.name = name;
		}
	}

	private NodeType nodeType;
	private Integer id;
	private String nodeId;
	private String ip;
	private String heartip;
	private String username;
	private String password;
	private int sshPort;
	private int appNodeServPort;
	private String idbdir;
	private int weight;
	private int appNodePort;
	private boolean appNodeStatus;
	private String systemVersion;
	private boolean running;

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public int getAppNodePort() {
		return appNodePort;
	}

	public void setAppNodePort(int appNodePort) {
		this.appNodePort = appNodePort;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHeartip() {
		if (StringUtils.isEmpty(heartip)) {
			return ip;
		} else {
			return heartip;
		}
	}

	public void setHeartip(String heartip) {
		this.heartip = heartip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public int getSshPort() {
		return sshPort;
	}

	public void setSshPort(int sshPort) {
		this.sshPort = sshPort;
	}

	public String getIdbdir() {
		return idbdir;
	}

	public void setIdbdir(String idbdir) {
		this.idbdir = idbdir;
	}

	public int getAppNodeServPort() {
		return appNodeServPort;
	}

	public void setAppNodeServPort(int appNodeServPort) {
		this.appNodeServPort = appNodeServPort;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public boolean isAppNodeStatus() {
		return appNodeStatus;
	}

	public void setAppNodeStatus(boolean appNodeStatus) {
		this.appNodeStatus = appNodeStatus;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof NodeVO)) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		NodeVO node = (NodeVO) obj;
		if (this.nodeId != null && node.getNodeId() != null
				&& this.nodeId.equals(node.getNodeId())) {
			return true;
		}
		if (ip != null && ip.equals(node.getIp())) {
			return true;
		}
		return false;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	@Override
	public String toString() {
		return nodeId + "[" + ip + "]";
	}

}
