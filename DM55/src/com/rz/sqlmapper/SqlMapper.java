package com.rz.sqlmapper;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.resource.StringTemplateResourceLoader;

import com.rz.common.Record;
import com.rz.dao.DB;

public class SqlMapper {
	static String lineSeparator = System.getProperty("line.separator", "\n");
	static Map<String, String> sqls = new LinkedHashMap<String, String>();
	static {
		try {
			URL url = SqlMapper.class.getClassLoader().getResource("sqlmapper");
			File f = new File(url.toURI());
			File[] sf = f.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith("sql");
				}
			});
			for (File file : sf) {
				// String fn = file.getName();
				// String ns = fn.substring(0, fn.lastIndexOf('.'));
				// String ft = FileUtils.readFileToString(file, "UTF-8");
				// String[] arr = ft.split("===");
				// for (String string : arr) {
				// String st = string.replaceAll("\r\n*\t*", " ")
				// .replaceAll("\\s{2,}", " ").trim();
				// System.out.println(st);
				// sqls.put(ns + "." + fn, "");
				// }
				readSqlFile(file);
//				for (Map.Entry<String, String> entry : sqls.entrySet()) {
//					String sql = entry.getValue();
//					Map<String, Object> s = parse(sql, "#{", "}");
//					System.out.println(s.get("sql"));
//					System.out.println(s.get("ps"));
//				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Load sql failed", e);
		}
	}

	public static void main(String[] args) {
		// ClasspathResourceLoader resourceLoader = new
		// ClasspathResourceLoader();
		// Configuration cfg = Configuration.defaultConfiguration();
		// cfg.setPlaceholderStart("${");
		// cfg.setPlaceholderEnd("}");
		// cfg.setStatementStart("@");
		// cfg.setStatementEnd(null);
		// GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
		// Template t = gt.getTemplate("");
		// String sqltext = t.render();
		// gt.setSharedVars(shared);
		List<String> ids = new ArrayList<String>();
		ids.add("3");
		ids.add("5");
		ids.add("7");
		 Map<String,Object> shared = new HashMap<String,Object>();
		 shared.put("name", "beetl");
		 shared.put("name", "IO");
		 shared.put("ids", ids);
		getSql("user.select2", shared);

	}

	public static void getSql(String id, Map<String, Object> params) {
		try {
			StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
			Configuration cfg = Configuration.defaultConfiguration();
			cfg.setEngine("com.rz.sqlmapper.SQLTemplateEngine");
			cfg.setPlaceholderStart("#{");
			cfg.setPlaceholderEnd("}");
			cfg.setStatementStart("@");
			cfg.setStatementEnd(null);
			GroupTemplate gt = new GroupTemplate(resourceLoader, cfg);
			gt.registerFunction("join" , new JoinFunction());
			gt.registerFunction("like" , new LikeFunction());
			String sqlTmpl = sqls.get(id);
			//System.out.println(sqlTmpl);
			Template t = gt.getTemplate(sqlTmpl);
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				String k = entry.getKey();
				Object v = entry.getValue();
				t.binding(k, v, true);
			}
			List<Object> jdbcPara = new LinkedList<Object>();
			t.binding("_paras", jdbcPara);
			String sqltext = t.render();
			Sql result = new Sql();
			result.sql = sqltext.replaceAll("\n\t*", " ").replaceAll("\\s{2,}", " ").trim().replaceAll("where 1=1 and", "where");
			result.params = jdbcPara;
			System.out.println(result.sql);
			System.out.println(result.params);
			DB db = DB.getInstance();
			List<Record> rs = db.find(result.sql, result.params.toArray());
			System.out.println(rs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// String sqltext = t.render();
		// Map<String,Object> shared = new HashMap<String,Object>();
		// //shared.put("name", "beetl");
		// gt.setSharedVars(shared);

	}

	private static void readSqlFile(File file) {
		String fn = file.getName();
		String ns = fn.substring(0, fn.lastIndexOf('.'));
		LinkedList<String> list = new LinkedList<String>();
		BufferedReader bf = null;
		try {
			FileInputStream ins = new FileInputStream(file);
			bf = new BufferedReader(new InputStreamReader(ins));
			String temp = null;
			StringBuilder sql = null;
			String key = null;
			int lineNum = 0;
			int findLineNum = 0;
			while ((temp = bf.readLine()) != null) {
				temp = temp.trim();
				lineNum++;
				if (temp.startsWith("===")) {// 读取到===号，说明上一行是key，下面是注释或者SQL语句
					if (!list.isEmpty() && list.size() > 1) {// 如果链表里面有多个，说明是上一句的sql+下一句的key
						String tempKey = list.pollLast();// 取出下一句sql的key先存着
						sql = new StringBuilder();
						key = list.pollFirst();
						while (!list.isEmpty()) {// 拼装成一句sql
							sql.append(list.pollFirst() + lineSeparator);
						}
						sqls.put(ns + "." + key, sql.toString().trim());
						list.addLast(tempKey);// 把下一句的key又放进来
						findLineNum = lineNum;
					}
					boolean sqlStart = false;
					String tempNext = null;
					while ((tempNext = bf.readLine()) != null) {// 处理注释的情况
						tempNext = tempNext.trim();
						lineNum++;
						if (tempNext.startsWith("*")) {// 读到注释行，不做任何处理
							continue;
						} else if (!sqlStart && tempNext.trim().length() == 0) {
							// 注释的空格
							continue;
						} else {
							sqlStart = true;
							list.addLast(tempNext);// ===下面不是*号的情况，是一条sql
							break;// 读到一句sql就跳出循环
						}
					}
				} else {
					list.addLast(temp);
				}
			}
			// 最后一句sql
			sql = new StringBuilder();
			key = list.pollFirst();
			while (!list.isEmpty()) {
				sql.append(list.pollFirst() + lineSeparator);
			}
			sqls.put(ns + "." + key, sql.toString().trim());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bf != null) {
				try {
					bf.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private static Map<String, Object> parse(String text, String openToken,
			String closeToken) {
		Map<String, Object> ret = new HashMap<String, Object>();
		StringBuilder builder = new StringBuilder();
		List<String> ps = new ArrayList<String>();
		if (text != null && text.length() > 0) {// 如果传入的字符串有值
												// 将字符串转为字符数组
			char[] src = text.toCharArray();
			int offset = 0;
			// 判断openToken在text中的位置，注意indexOf函数的返回值-1表示不存在，0表示在在开头的位置
			int start = text.indexOf(openToken, offset);
			while (start > -1) {
				if (start > 0 && src[start - 1] == '\\') {
					// 如果text中在openToken前存在转义符就将转义符去掉。如果openToken前存在转义符，start的值必然大于0，最小也为1
					// 因为此时openToken是不需要进行处理的，所以也不需要处理endToken。接着查找下一个openToken
					builder.append(src, offset, start - 1).append(openToken);
					offset = start + openToken.length();// 重设offset
				} else {
					int end = text.indexOf(closeToken, start + 1);
					if (end == -1) {// 如果不存在openToken，则直接将offset位置后的字符添加到builder中
						builder.append(src, offset, src.length - offset);
						offset = src.length;// 重设offset
					} else {
						builder.append(src, offset, start - offset);// 添加openToken前offset后位置的字符到bulider中
						offset = start + openToken.length();// 重设offset
						String content = new String(src, offset, end - offset);// 获取openToken和endToken位置间的字符串
						// builder.append(handler.handleToken(content));//调用handler进行处理
						ps.add(content);
						builder.append("?");// 调用handler进行处理
						offset = end + closeToken.length();// 重设offset
					}
				}
				start = text.indexOf(openToken, offset);// 开始下一个循环
			}
			// 只有当text中不存在openToken且text.length大于0时才会执行下面的语句
			if (offset < src.length) {
				builder.append(src, offset, src.length - offset);
			}
		}
		ret.put("sql", builder.toString());
		ret.put("ps", ps);
		return ret;
	}
}
